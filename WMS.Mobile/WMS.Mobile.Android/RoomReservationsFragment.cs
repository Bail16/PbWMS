﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using WMS.Mobile.Models;
using WMS.Mobile.Extensions;
using System.Threading.Tasks;
using WMS.Mobile.Http.Services;
using Android.Support.Design.Widget;
using WMS.Mobile.Droid.Globals;

namespace WMS.Mobile.Droid
{
    public class RoomReservationsFragment : Fragment, DatePickerDialog.IOnDateSetListener
    {
        private RecyclerView _recyclerView;
        private RecyclerView.LayoutManager _layoutManager;  
        private RoomReservationsAdapter _reservationsAdapter;
        private TextView _tvDatePicker;
        private FloatingActionButton _floatingButton;

        private RoomService _roomService;
        private StorageService _storageService;

        private Task<ActualDateTimeResult> _actualDatePromise;
        private DateTime _selectedDate;
        private Guid _roomId;

        public RoomReservationsFragment(Guid roomId)
        {
            _roomId = roomId;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _roomService = new RoomService();
            _actualDatePromise = _roomService.GetActualDateTime();
            _storageService = new StorageService(Activity.BaseContext);

            // Create your fragment here
        }
        public override async void OnViewCreated(View view, Bundle savedInstanceState)
        {
            await SetDefaultValues();
            ApplyReservationsByDate(_selectedDate, _roomId);
            base.OnViewCreated(view, savedInstanceState);
        }
    
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.ReservationsLayout, container, false);
            
            RegisterAdapter(view);
            GetResources(view);
            _floatingButton.Click += _floatingButton_Click;

            _tvDatePicker.Click += _datePicker_Click;
            return view;
        }

        private void _floatingButton_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private async void _datePicker_Click(object sender, EventArgs e)
        {
            var actualDate = await _roomService.GetActualDateTime();
            var datePickerDialog = new DatePickerDialog(Activity, this, _selectedDate.Year, _selectedDate.Month - 1, _selectedDate.Day);
            //datePickerDialog.DatePicker.MinDate = (long)actualDate.ActualDateTime.ToUniversalTime().Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds;
            ShowDialog(datePickerDialog);
        }
        private void ShowDialog(DatePickerDialog dialog)
        {
            dialog.Show();
        }
        public async void OnDateSet(DatePicker view, int year, int month, int dayOfMonth)
        {
            var actualDate = await _roomService.GetActualDateTime();
            _selectedDate = new DateTime(year, month + 1, dayOfMonth);
            _tvDatePicker.Text = _selectedDate.ToDatePickerString();
            ApplyReservationsByDate(_selectedDate, _roomId);
            if (_selectedDate.Date < actualDate.ActualDateTime.Date && _floatingButton.Visibility == ViewStates.Visible)
            {
                _floatingButton.Hide();
            }
            else if(_floatingButton.Visibility == ViewStates.Gone && _selectedDate.Date >= actualDate.ActualDateTime.Date)
            {
                _floatingButton.Show();
            }
        }

        private async Task SetDefaultValues()
        {
            var actualDateResult = await _actualDatePromise;
            _selectedDate = actualDateResult.ActualDateTime;
            _tvDatePicker.Text = _selectedDate.ToDatePickerString();
        }

        private async void ApplyReservationsByDate(DateTime selectedDate, Guid roomId)
        {
            var userId = _storageService.UserId;
            if (userId.HasValue)
            {
                var reservationsResult = await _roomService.GetRoomReservations(roomId, selectedDate);
                switch (reservationsResult.State)
                {
                    case HttpDefaultCodes.ok:
                        _reservationsAdapter._reservations = reservationsResult.Reservations;
                        _reservationsAdapter.NotifyDataSetChanged();
                        break;
                    case HttpDefaultCodes.error:
                        Toast.MakeText(Context, "Error while loading data", ToastLength.Long).Show();
                        break;
                    default:
                        break;
                }
            }
            else
                Activity.StartActivity(typeof(LoginActivity));
        }


        private void GetResources(View view)
        {
            _tvDatePicker = view.FindViewById<TextView>(Resource.Id.tv_reservation_selecteddate);
            _floatingButton = view.FindViewById<FloatingActionButton>(Resource.Id.reservation_fab);
        }

        private void RegisterAdapter(View view)
        {
            _recyclerView = view.FindViewById<RecyclerView>(Resource.Id.reservation_recyclerView);
            _layoutManager = new LinearLayoutManager(_recyclerView.Context);
            _recyclerView.SetLayoutManager(_layoutManager);
            _reservationsAdapter = new RoomReservationsAdapter();
            _recyclerView.SetAdapter(_reservationsAdapter);
        }

      
    }
}