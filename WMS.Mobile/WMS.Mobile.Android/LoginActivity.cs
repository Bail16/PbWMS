﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.Design.Widget;
using WMS.Mobile.Mappers;
using WMS.Mobile.Http;
using WMS.Mobile.Droid.Globals;
using Android.Support.Constraints;
using Android.Graphics.Drawables;
using WMS.Mobile.Models;
using Android.Accounts;
using Java.Security;

namespace WMS.Mobile.Droid
{
    [Activity(Label = "PB WMS", Theme = "@style/AppTheme", Icon = "@drawable/ic_app", MainLauncher = true, NoHistory = true)]
    public class LoginActivity : MyActivity
    {
        private ValidationComponent _inputValidator;
        private ProgressBar _loggingInBar;

        EditText _inputEmail;
        EditText _inputSecretKey;

        Button _btnLogin;

        public LoginActivity()
        {
            _inputValidator = new ValidationComponent();
        }

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            var isLogged = false;
            if (_storageService.Email != null && _storageService.SecretKey != null && _storageService.Pin != null)
            {
                var loginResult = await TryLoginOnStart();
                if (loginResult.State == HttpLoginCodes.ok)
                {
                    isLogged = true;
                    SaveUserSecondLogin(loginResult);                    
                    StartActivity(typeof(EnterPinActivity));
                }
            }
            if (!isLogged)
            {
                SetContentView(Resource.Layout.LoginLayout);

                SetBackground();
                GetResources();
                SetInputDefault();

                _btnLogin.Click += Btn_login_Click;
            }         
        }

        private void SetInputDefault()
        {
            _inputEmail.Text = "xkzemczak@gmail.com";
            _inputSecretKey.Text = "dd6jjw";
        }

        private void GetResources()
        {
            _inputEmail = FindViewById<EditText>(Resource.Id.input_email);
            _inputSecretKey = FindViewById<EditText>(Resource.Id.input_secretKey);
            _btnLogin = FindViewById<Button>(Resource.Id.btn_login);
            _loggingInBar = FindViewById<ProgressBar>(Resource.Id.progressBar_LoggingIn);
        }

        private async void Btn_login_Click(object sender, EventArgs e)
        {
            var isEmailOk = ValidateEmailField(_inputEmail);
            var isKeyOk = ValidateSecretKeyField(_inputSecretKey);

            if (isEmailOk && isKeyOk)
            {
                SetLoggingInLoader(true);
                var loginResult = await _authService.LoginAsync(_inputEmail.Text, _inputSecretKey.Text, _storageService.ApiClientSecretKey);
                switch (loginResult.State)
                {
                    case HttpLoginCodes.ok:
                        Toast.MakeText(this, "Success", ToastLength.Short).Show();
                        SaveUserInformations(loginResult);
                        StartActivity(typeof(CreatePinActivity));
                        break;
                    case HttpLoginCodes.badEmailOrCode:
                        Toast.MakeText(this, "Bad email or key", ToastLength.Long).Show();
                        break;
                    case HttpLoginCodes.connectionError:
                        Toast.MakeText(this, "Connection error", ToastLength.Long).Show();
                        break;
                    default:
                        break;
                }
            }
            SetLoggingInLoader(false);
        }

        private void SaveUserInformations(LoginResult loginResult)
        {
            _storageService.UserId = loginResult.UserId;
            _storageService.Name = loginResult.Name;
            _storageService.Surname = loginResult.Surname;
            _storageService.Email = _inputEmail.Text;
            _storageService.SecretKey = _inputSecretKey.Text;
        }

        private void SaveUserSecondLogin(LoginResult loginResult)
        {
            _storageService.UserId = loginResult.UserId;
            _storageService.Name = loginResult.Name;
            _storageService.Surname = loginResult.Surname;
        }

        private bool ValidateSecretKeyField(EditText inputSecretKey)
        {
            var secretKeyInpLayout = FindViewById<TextInputLayout>(Resource.Id.inputLayout_secretKey);
            if (!_inputValidator.ValidateSecretKey(inputSecretKey.Text))
            {
                var error = GetString(Resource.String.err_secretKey);
                secretKeyInpLayout.Error = error;
                secretKeyInpLayout.ErrorEnabled = true;
                return false;
            }
            else
            {
                secretKeyInpLayout.ErrorEnabled = false;
                return true;
            }
        }

        private bool ValidateEmailField(EditText inputEmail)
        {
            var emailInpLayout = FindViewById<TextInputLayout>(Resource.Id.inputLayout_email);
            if (!_inputValidator.ValidateEmail(inputEmail.Text))
            {
                var error = GetString(Resource.String.err_badEmail);
                emailInpLayout.Error = error;
                emailInpLayout.ErrorEnabled = true;
                return false;
            }
            else
            {
                emailInpLayout.ErrorEnabled = false;
                return true;
            }
        }

        private void SetBackground()
        {
            LinearLayout container = FindViewById<LinearLayout>(Resource.Id.LoginLayout);

            container.SetBackgroundResource(Resource.Drawable.background_animation);

            AnimationDrawable anim = (AnimationDrawable)container.Background;
            anim.SetEnterFadeDuration(4000);
            anim.SetExitFadeDuration(6000);
            anim.Start();
        }

        private void SetLoggingInLoader(bool state)
        {
            if (state)
            {
                _btnLogin.Visibility = ViewStates.Invisible;
                _loggingInBar.Visibility = ViewStates.Visible;
            }
            else
            {
                _btnLogin.Visibility = ViewStates.Visible;
                _loggingInBar.Visibility = ViewStates.Invisible;
            }
        }
    }
}