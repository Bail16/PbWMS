﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Com.Goodiebag.Pinview;
using Android.Graphics.Drawables;
using Android.Support.V4.Content;
using System.Drawing;

namespace WMS.Mobile.Droid
{
    [Activity(Label = "CreatePinActivity")]
    public class CreatePinActivity : MyActivity, Pinview.IPinViewEventListener
    {
        private Pinview _pinViewFirst;
        private Pinview _pinViewSecond;

        public void OnDataEntered(Pinview p0, bool p1)
        {
            if(p0.Id == Resource.Id.pinViewFirst)
            {
                _pinViewSecond.RequestFocus();
            }
            else
            {
                if(_pinViewFirst.Value == p0.Value)
                {
                    _storageService.Pin = _pinViewFirst.Value;
                    var activityToStart = GetIntentWithoutHistory(this, typeof(DrawerMainActivity));
                    StartActivity(activityToStart);
                    CloseKeyboard();
                }
                else
                {
                    var activityToStart = GetIntentWithoutHistory(this, typeof(CreatePinActivity));
                    activityToStart.PutExtra("PinsNotCorrect", true);
                    StartActivity(activityToStart);
                }
                Finish();
            }
            
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.CreatePinLayout);
            SetTitleToAcionBar(GetString(Resource.String.createpin_title), Resource.Drawable.ic_create_black_24dp, FindViewById<TextView>(Resource.Id.lbl_fragmenttitle));
            //FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.app_bar).SetBackgroundColor(new Android.Graphics.Color(ContextCompat.GetColor(this, Resource.Color.colorPrimaryDark)));
            _pinViewFirst = FindViewById<Pinview>(Resource.Id.pinViewFirst);
            _pinViewSecond = FindViewById<Pinview>(Resource.Id.pinViewSecond);
            _pinViewFirst.SetPinViewEventListener(this);
            _pinViewSecond.SetPinViewEventListener(this);

            bool pinsNotCorrect = Intent.GetBooleanExtra("PinsNotCorrect", false);
            if (pinsNotCorrect)
            {
                var builder = new AlertDialog.Builder(this);
                builder.SetMessage("Pins not match");
                builder.Show();
            }
        }

    }
}