﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Support.V7.Widget;
using WMS.Mobile.Droid.Globals;
using WMS.Mobile.Http;
using WMS.Mobile.Models;
using WMS.Mobile.Extensions;
using System.Globalization;
using System.Threading.Tasks;

namespace WMS.Mobile.Droid
{
    public class HistoryFragment : Fragment, DatePickerDialog.IOnDateSetListener
    {
        private RecyclerView _recyclerView;
        private RecyclerView.LayoutManager _layoutManager;
        private HistoryAdapter _historyAdapter;
            
        private TimeLogService _timeLogService;
        private Task<ActualDateTimeResult> _actualDatePromise;
        private StorageService _storageService;
        private TextView _tvDayWorkTime;
        private TextView _tvDatePicker;

        private DateTime _selectedDate;



        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _timeLogService = new TimeLogService();
            _actualDatePromise = _timeLogService.GetActualDateTime();
            _storageService = new StorageService(Activity.BaseContext);

        }
        public override async void OnViewCreated(View view, Bundle savedInstanceState)
        {
            await SetDefaultValues();
            ApplyTimeLogsByDate(_selectedDate);
            base.OnViewCreated(view, savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.HistoryLayout, container, false);

            RegisterAdapter(view);
            GetResources(view);

            _tvDatePicker.Click += _datePicker_Click;
            return view;
        }


        private async Task SetDefaultValues()
        {
            var actualDateResult = await _actualDatePromise;
            _selectedDate = actualDateResult.ActualDateTime;
            _tvDatePicker.Text = _selectedDate.ToDatePickerString();
            _tvDayWorkTime.Text = new TimeSpan().ToWorkOverTimeString();
        }

        private void RegisterAdapter(View view)
        {
            _recyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerView_timelogs);
            _layoutManager = new LinearLayoutManager(_recyclerView.Context);
            _recyclerView.SetLayoutManager(_layoutManager);
            _historyAdapter = new HistoryAdapter();
            _recyclerView.SetAdapter(_historyAdapter);
        }

        private void GetResources(View view)
        {
            _tvDayWorkTime = view.FindViewById<TextView>(Resource.Id.tv_totalworktime);
            _tvDatePicker = view.FindViewById<TextView>(Resource.Id.tv_selecteddate);
        }

        private async void _datePicker_Click(object sender, EventArgs e)
        {
            var actualDate = await _timeLogService.GetActualDateTime();
            var datePickerDialog = new DatePickerDialog(Activity, this, _selectedDate.Year, _selectedDate.Month - 1, _selectedDate.Day);
            datePickerDialog.DatePicker.MaxDate = (long)actualDate.ActualDateTime.ToUniversalTime().Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds;
            ShowDialog(datePickerDialog);
        }

        private void ShowDialog(DatePickerDialog dialog)
        {
            dialog.Show();
        }


        private async void ApplyTimeLogsByDate(DateTime date)
        {
            var actualDate = await _timeLogService.GetActualDateTime();
            var userId = _storageService.UserId;
            if (userId.HasValue)
            {
                var timeLogsResult = await _timeLogService.GetTimeLogs(_storageService.UserId.Value, date, date, actualDate.ActualDateTime);
                switch (timeLogsResult.State)
                {
                    case HttpDefaultCodes.ok:
                        _tvDayWorkTime.Text = timeLogsResult.DayWorkTime.ToWorkOverTimeString();
                        _historyAdapter._timeLogs = timeLogsResult.TimeLogs;
                        _historyAdapter.NotifyDataSetChanged();
                        break;
                    case HttpDefaultCodes.error:
                        Toast.MakeText(Context, "Error while loading data", ToastLength.Long).Show();
                        break;
                    default:
                        break;
                }
            }
            else
                Activity.StartActivity(typeof(LoginActivity));
        }

        public void OnDateSet(DatePicker view, int year, int month, int dayOfMonth)
        {
            _selectedDate = new DateTime(year, month + 1, dayOfMonth);
            _tvDatePicker.Text = _selectedDate.ToDatePickerString();
            ApplyTimeLogsByDate(_selectedDate);
        }
    }
}