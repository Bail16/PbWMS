﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Preferences;
using Java.Security;
using System.Timers;

namespace WMS.Mobile.Droid.Globals
{
    public class StorageService
    {
        private static ISharedPreferences _settings;
        private static Context _context;
        public StorageService(Context baseContext)
        {
            _context = _context ?? baseContext;
            _settings = _settings ?? _context.GetSharedPreferences("settings", FileCreationMode.Private);
        }
        public string ApiClientSecretKey => "3ea737c3-fb55-4849-8c46-42e9908b374d";
        public string Pin
        {
            get
            {
                var pin = _settings.GetString("UserPin", "");
                if (!string.IsNullOrEmpty(pin))
                    return pin;
                else return null;
            }
            set
            {
                var editor = _settings.Edit();
                editor.PutString("UserPin", value);
                editor.Commit();
            }
        }

        public Guid? ActiveTimeLogId
        {
            get
            {
                var activeTimeLogId = _settings.GetString("ActiveTimeLogId", null);
                if (!string.IsNullOrEmpty(activeTimeLogId))
                    return Guid.Parse(activeTimeLogId);
                else return null;
            }
            set
            {
                var editor = _settings.Edit();
                editor.PutString("ActiveTimeLogId", value.ToString());
                editor.Commit();
            }
        }
        public string Email
        {
            get
            {
                var userEmail = _settings.GetString("UserEmail", null);
                return userEmail;
            }
            set
            {
                var editor = _settings.Edit();
                editor.PutString("UserEmail", value);
                editor.Commit();
            }
        }
        public string SecretKey
        {
            get
            {
                var userSecretKey = _settings.GetString("UserSecretKey", null);
                return userSecretKey;
            }
            set
            {
                var editor = _settings.Edit();
                editor.PutString("UserSecretKey", value);
                editor.Commit();
            }
        }

        private static string _name;
        private static string _surname;
        private static Guid? _userId;
        public string Name { get { return _name; } set { _name = value; } }
        public string Surname { get { return _surname; } set { _surname = value; } }
        public Guid? UserId { get { return _userId; } set { _userId = value; } }

        public string NameAndSurname => Name + " " + Surname;
    }
}