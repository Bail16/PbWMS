﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using WMS.Mobile.Droid.Globals;

namespace WMS.Mobile.Droid
{
    [Activity(Label = "DrawerMainActivity")]
    public class DrawerMainActivity : MyActivity
    {
        DrawerLayout _drawerLayout;
        TextView _actionBarTitle;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.MainDrawer);
            _drawerLayout = FindViewById<DrawerLayout>(Resource.Id.MainDrawer);
         

            // Init toolbar    
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.app_bar);
            _actionBarTitle = FindViewById<TextView>(Resource.Id.lbl_fragmenttitle);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowHomeEnabled(true);
            SetTitleToAcionBar(GetString(Resource.String.dashboard_title), Resource.Drawable.ic_home_white_small, _actionBarTitle);

            // Attach item selected handler to navigation view
            var navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navigationView.NavigationItemSelected += NavigationView_NavigationItemSelected;

            // Create ActionBarDrawerToggle button and add it to the toolbar
            var drawerToggle = new ActionBarDrawerToggle(this, _drawerLayout, toolbar, Resource.String.open_drawer, Resource.String.close_drawer);
            //drawerLayout.SetDrawerListener(drawerToggle);
            drawerToggle.SyncState();

            //load default home screen
            var ft = FragmentManager.BeginTransaction();
            ft.AddToBackStack(null);
            ft.Add(Resource.Id.HomeFrameLayout, new DashboardFragment());
            ft.Commit();

            
            navigationView.GetHeaderView(0).FindViewById<TextView>(Resource.Id.lbl_namesurname).Text = _storageService.NameAndSurname;
            navigationView.GetHeaderView(0).FindViewById<TextView>(Resource.Id.lbl_email).Text = _storageService.Email;


        }
        //define custom title text
        protected override void OnResume()
        {
            SupportActionBar.SetTitle(Resource.String.app_name);
            base.OnResume();
        }

        protected override void OnRestart()
        {
            base.OnRestart();

            StartActivity(typeof(EnterPinActivity));
        }

        //define action for navigation menu selection
        void NavigationView_NavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            var ft = FragmentManager.BeginTransaction();
            switch (e.MenuItem.ItemId)
            {
                case (Resource.Id.nav_home):
                    SetTitleToAcionBar(GetString(Resource.String.dashboard_title), Resource.Drawable.ic_home_white_small, _actionBarTitle);
                    ft.AddToBackStack(null);
                    ft.Replace(Resource.Id.HomeFrameLayout, new DashboardFragment());             
                    break;
                case (Resource.Id.nav_history):
                    SetTitleToAcionBar(GetString(Resource.String.timelogs_title), Resource.Drawable.ic_history_black_24dp, _actionBarTitle);
                    ft.AddToBackStack(null);
                    ft.Replace(Resource.Id.HomeFrameLayout, new HistoryFragment());                
                    break;
                case (Resource.Id.nav_rooms):
                    SetTitleToAcionBar(GetString(Resource.String.rooms_title), Resource.Drawable.ic_lock_outline_black_24dp, _actionBarTitle);
                    ft.AddToBackStack(null);
                    ft.Replace(Resource.Id.HomeFrameLayout, new RoomsFragment());
                    break;

                case (Resource.Id.nav_logout):
                    Logout();
                    break;
                default:
                    break;
            }
            ft.Commit();
            _drawerLayout.CloseDrawers();
        }

        //add custom icon to tolbar
        public override bool OnCreateOptionsMenu(Android.Views.IMenu menu)
        {           
            return base.OnCreateOptionsMenu(menu);
        }
        //define action for tolbar icon press
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    //this.Activity.Finish();
                    return true;             
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
        //to avoid direct app exit on backpreesed and to show fragment from stack
        public override void OnBackPressed()
        {
            if (FragmentManager.BackStackEntryCount != 0)
            {
                FragmentManager.PopBackStack();// fragmentManager.popBackStack();
            }
            else
            {
                base.OnBackPressed();
            }
        }

        public void SetTitleToActionBar(string title, int drawable)
        {
            SetTitleToAcionBar(title, Resource.Drawable.ic_lock_outline_black_24dp, _actionBarTitle);
        }

        
    }
}