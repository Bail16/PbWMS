﻿using System;

using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using System.Collections.Generic;
using System.Runtime.Remoting.Contexts;
using WMS.Mobile.Models;
using WMS.Mobile.Extensions;

namespace WMS.Mobile.Droid
{
    public class HistoryAdapter : RecyclerView.Adapter
    {
        public event EventHandler<HistoryAdapterClickEventArgs> ItemClick;
        public event EventHandler<HistoryAdapterClickEventArgs> ItemLongClick;
        
        public List<GetTimeLogResult> _timeLogs;

        public HistoryAdapter()
        {
            _timeLogs = new List<GetTimeLogResult>();
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            //Setup your layout here
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.HistoryCard, parent, false);
            
            //var id = Resource.Layout.__YOUR_ITEM_HERE;
            //itemView = LayoutInflater.From(parent.Context).
            //       Inflate(id, parent, false);

            var vh = new HistoryAdapterViewHolder(itemView, OnClick, OnLongClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var item = _timeLogs[position];

            // Replace the contents of the view with that element
            var holder = viewHolder as HistoryAdapterViewHolder;

            holder.TvTimeLogNumber.Text = (position + 1).ToString();
            holder.TvEntryTime.Text = item.EntryTime.ToHourString();
            holder.TvLeaveTime.Text = item.LeaveTime.ToHourString();
            holder.TvWorkTime.Text = item.WorkTime.ToWorkOverTimeString();
            holder.TvTimeLogType.Text = item.TypeCamelCaseName;

            if(position == 0)
            {
                RecyclerView.MarginLayoutParams lp = (RecyclerView.MarginLayoutParams)holder.ItemView.LayoutParameters;
                lp.SetMargins(lp.LeftMargin, 0, lp.RightMargin, lp.BottomMargin);
                holder.ItemView.LayoutParameters = lp;
            }
           
            //holder.TextView.Text = items[position];
        }
        

        public override int ItemCount => _timeLogs.Count;

        void OnClick(HistoryAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(HistoryAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);

    }

    public class HistoryAdapterViewHolder : RecyclerView.ViewHolder
    {        
        public TextView TvEntryTime { get; set; }
        public TextView TvLeaveTime { get; set; }
        public TextView TvWorkTime { get; set; }
        public TextView TvTimeLogType { get; set; }
        public TextView TvTimeLogNumber { get; set; }


        public HistoryAdapterViewHolder(View itemView, Action<HistoryAdapterClickEventArgs> clickListener,
                            Action<HistoryAdapterClickEventArgs> longClickListener) : base(itemView)
        {

            TvEntryTime = itemView.FindViewById<TextView>(Resource.Id.tv_entrytime);
            TvLeaveTime = itemView.FindViewById<TextView>(Resource.Id.tv_leavetime);
            TvWorkTime = itemView.FindViewById<TextView>(Resource.Id.tv_worktime);
            TvTimeLogType = itemView.FindViewById<TextView>(Resource.Id.tv_timelogtype);
            TvTimeLogNumber = itemView.FindViewById<TextView>(Resource.Id.tv_timelognum);
            
            itemView.Click += (sender, e) => clickListener(new HistoryAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new HistoryAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
        }             
    }

    public class HistoryAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }    
}