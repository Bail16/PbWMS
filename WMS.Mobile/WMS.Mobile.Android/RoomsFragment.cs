﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using WMS.Mobile.Http.Services;
using WMS.Mobile.Models;
using Android.Support.V7.Widget;
using System.Threading.Tasks;

namespace WMS.Mobile.Droid
{
    public class RoomsFragment : Fragment
    {
        private RecyclerView _recyclerView;
        private RecyclerView.LayoutManager _layoutManager;
        private RoomsAdapter _roomsAdapter;

        private RoomService _roomsService;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _roomsService = new RoomService();
            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.RoomsLayout, container, false);
            RegisterAdapter(view);

            return view;
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            ApplyRooms();

            base.OnViewCreated(view, savedInstanceState);
        }

        private void RegisterAdapter(View view)
        {
            _recyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerView_rooms);
            _layoutManager = new LinearLayoutManager(_recyclerView.Context);
            _recyclerView.SetLayoutManager(_layoutManager);
            _roomsAdapter = new RoomsAdapter();
            _roomsAdapter.ItemClick += _roomsAdapter_ItemClick;
            _recyclerView.SetAdapter(_roomsAdapter);
        }

        private void _roomsAdapter_ItemClick(object sender, RoomsAdapterClickEventArgs e)
        {
            var ft = FragmentManager.BeginTransaction();
            var activity = Activity as DrawerMainActivity;
            activity.SetTitleToActionBar(e.RoomName, Resource.Drawable.ic_lock_outline_black_24dp);
            ft.AddToBackStack(null);
            ft.Replace(Resource.Id.HomeFrameLayout, new RoomReservationsFragment(e.RoomId));
            ft.Commit();
        }

        private async void ApplyRooms()
        {
            var roomsResult = await _roomsService.GetAllRooms();            
            switch (roomsResult.State)
            {
                case HttpDefaultCodes.ok:
                    _roomsAdapter._rooms = roomsResult.Rooms;
                    _roomsAdapter.NotifyDataSetChanged();
                    break;
                case HttpDefaultCodes.error:
                    Toast.MakeText(Context, "Error while loading data", ToastLength.Long).Show();
                    break;
                default:
                    break;
            }

        }
    }
}