﻿using System;

using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using System.Collections.Generic;
using System.Runtime.Remoting.Contexts;
using WMS.Mobile.Models;
using WMS.Mobile.Extensions;

namespace WMS.Mobile.Droid
{
    public class RoomsAdapter : RecyclerView.Adapter
    {
        public event EventHandler<RoomsAdapterClickEventArgs> ItemClick;
        public event EventHandler<RoomsAdapterClickEventArgs> ItemLongClick;

        public List<GetRoomResult> _rooms;

        public RoomsAdapter()
        {
            _rooms = new List<GetRoomResult>();
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            //Setup your layout here
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.RoomCard, parent, false);

            //var id = Resource.Layout.__YOUR_ITEM_HERE;
            //itemView = LayoutInflater.From(parent.Context).
            //       Inflate(id, parent, false);

            var vh = new RoomsAdapterViewHolder(itemView, OnClick, OnLongClick);
            return vh;
        }

        
        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var room = _rooms[position];

            // Replace the contents of the view with that element
            var holder = viewHolder as RoomsAdapterViewHolder;

            holder.TvRoomNumber.Text = (position + 1).ToString();
            holder.TvRoomName.Text = room.RoomName;
            holder.RoomId = room.RoomId;
            holder.RoomName = room.RoomName;

            if (position == 0)
            {
                RecyclerView.MarginLayoutParams lp = (RecyclerView.MarginLayoutParams)holder.ItemView.LayoutParameters;
                lp.SetMargins(lp.LeftMargin, 0, lp.RightMargin, lp.BottomMargin);
                holder.ItemView.LayoutParameters = lp;
            }

            //holder.TextView.Text = items[position];
        }


        public override int ItemCount => _rooms.Count;

        void OnClick(RoomsAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(RoomsAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);

    }

    public class RoomsAdapterViewHolder : RecyclerView.ViewHolder
    {
        public TextView TvRoomName { get; set; }
        public TextView TvRoomNumber { get; set; }
        public Guid RoomId { get;  set; }
        public string RoomName { get; set; }    


        public RoomsAdapterViewHolder(View itemView, Action<RoomsAdapterClickEventArgs> clickListener,
                            Action<RoomsAdapterClickEventArgs> longClickListener) : base(itemView)
        {
            TvRoomName = itemView.FindViewById<TextView>(Resource.Id.tv_roomname);
            TvRoomNumber = itemView.FindViewById<TextView>(Resource.Id.tv_roomNum); 

            itemView.Click += (sender, e) => clickListener(new RoomsAdapterClickEventArgs { View = itemView, Position = AdapterPosition,RoomName = RoomName, RoomId = RoomId });
            itemView.LongClick += (sender, e) => longClickListener(new RoomsAdapterClickEventArgs { View = itemView, Position = AdapterPosition, RoomName = RoomName, RoomId = RoomId });
        }
    }

    public class RoomsAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
        public Guid RoomId { get; set; }
        public string RoomName { get; set; }
    }
}