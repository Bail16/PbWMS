﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using WMS.Mobile.Droid.Globals;

namespace WMS.Mobile.Droid
{
    [Activity(Label = "@string/app_name", Icon ="@drawable/ic_app")]
    public class StartupActivity : MyActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            if(_storageService.UserId != null)
            {
                StartActivity(typeof(DrawerMainActivity));
            }
            else
            {
                StartActivity(typeof(LoginActivity));
                //StartActivity(typeof(DrawerMainActivity));
            }
        }
    }
}