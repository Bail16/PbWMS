﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Com.Goodiebag.Pinview;
using Android.Views.InputMethods;
using Android.Graphics.Drawables;
using Android.Support.V4.Content;

namespace WMS.Mobile.Droid
{
    [Activity(Label = "EnterPinActivity")]
    public class EnterPinActivity : MyActivity, Pinview.IPinViewEventListener
    {
        private Pinview _pinEntry;

        public void OnDataEntered(Pinview p0, bool p1)
        {
            var pin = _storageService.Pin;
            
            if(p0.Value == pin)
            {
                var activityToStart = GetIntentWithoutHistory(this, typeof(DrawerMainActivity));         
                StartActivity(activityToStart);
                CloseKeyboard();
            }
            else
            {                
                var activityToStart = GetIntentWithoutHistory(this, typeof(EnterPinActivity));
                activityToStart.PutExtra("IsBadPin", true);
                StartActivity(activityToStart);             
            }
            Finish();
        }

      

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.EnterPinLayout);
            SetTitleToAcionBar(GetString(Resource.String.enterpin_title), Resource.Drawable.ic_input_black_24dp, FindViewById<TextView>(Resource.Id.lbl_fragmenttitle));
            _pinEntry = FindViewById<Pinview>(Resource.Id.enterPinPinView);
            _pinEntry.SetPinViewEventListener(this);

            bool wasBadPin = Intent.GetBooleanExtra("IsBadPin", false);
            if (wasBadPin)
            {
                var builder = new AlertDialog.Builder(this);
                builder.SetMessage("Bad pin");
                builder.Show();
            }
        }

    }
}