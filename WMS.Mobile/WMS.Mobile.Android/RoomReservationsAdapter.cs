﻿using System;

using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using System.Collections.Generic;
using WMS.Mobile.Models;
using WMS.Mobile.Extensions;

namespace WMS.Mobile.Droid
{
    class RoomReservationsAdapter : RecyclerView.Adapter
    {
        public event EventHandler<RoomReservationsAdapterClickEventArgs> ItemClick;
        public event EventHandler<RoomReservationsAdapterClickEventArgs> ItemLongClick;

        public List<GetRoomReservationResult> _reservations; 

        public RoomReservationsAdapter()
        {
            _reservations = new List<GetRoomReservationResult>();
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            //Setup your layout here
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.ReservationCard, parent, false);
            //var id = Resource.Layout.__YOUR_ITEM_HERE;
            //itemView = LayoutInflater.From(parent.Context).
            //       Inflate(id, parent, false);

            var vh = new RoomReservationsAdapterViewHolder(itemView, OnClick, OnLongClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var reservation = _reservations[position];

            // Replace the contents of the view with that element
            var holder = viewHolder as RoomReservationsAdapterViewHolder;
            holder.TvReservationFrom.Text = reservation.TimeFrom.ToHourString();
            holder.TvReservationTo.Text = reservation.TimeTo.ToHourString();
            holder.TvReservedAt.Text = reservation.ReservationDoneAt.ToDatePickerString();
            holder.TvReservatorNameAndSurname.Text = reservation.UserName + " " + reservation.UserSurname;

            if (position == 0)
            {
                RecyclerView.MarginLayoutParams lp = (RecyclerView.MarginLayoutParams)holder.ItemView.LayoutParameters;
                lp.SetMargins(lp.LeftMargin, 0, lp.RightMargin, lp.BottomMargin);
                holder.ItemView.LayoutParameters = lp;
            }
        }

        public override int ItemCount => _reservations.Count;

        void OnClick(RoomReservationsAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(RoomReservationsAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);

    }

    public class RoomReservationsAdapterViewHolder : RecyclerView.ViewHolder
    {
        public TextView TvReservationFrom { get; set; }
        public TextView TvReservationTo { get; set; }   
        public TextView TvReservedAt { get; set; }


        public TextView TvReservatorNameAndSurname { get; set; }


        public RoomReservationsAdapterViewHolder(View itemView, Action<RoomReservationsAdapterClickEventArgs> clickListener,
                            Action<RoomReservationsAdapterClickEventArgs> longClickListener) : base(itemView)
        {
            TvReservationFrom = itemView.FindViewById<TextView>(Resource.Id.tv_hourFrom);
            TvReservationTo = itemView.FindViewById<TextView>(Resource.Id.tv_hourTo);
            TvReservedAt = itemView.FindViewById<TextView>(Resource.Id.tv_reservationAt);
            TvReservatorNameAndSurname = itemView.FindViewById<TextView>(Resource.Id.tv_reservedBy);

            itemView.Click += (sender, e) => clickListener(new RoomReservationsAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new RoomReservationsAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
        }
    }

    public class RoomReservationsAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}