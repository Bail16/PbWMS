﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using WMS.Mobile.Droid.Globals;
using WMS.Mobile.Http;
using WMS.Mobile.Models;
using System.Threading.Tasks;
using Android.Views.InputMethods;

namespace WMS.Mobile.Droid
{
    [Activity(Label = "MyActivity")]
    public class MyActivity : AppCompatActivity
    {
        protected StorageService _storageService;
        protected AuthService _authService;

        public MyActivity()
        {
            _authService = new AuthService();
        }
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _storageService = new StorageService(BaseContext);

            // Create your application here
        }

        protected void Logout()
        {
            _storageService.UserId = null;
            _storageService.Email = null;
            _storageService.SecretKey = null;
            _storageService.Pin = null;
            var activityToStart = GetIntentWithoutHistory(this, typeof(LoginActivity));
            StartActivity(activityToStart);
            Finish();
        }
        protected void CloseKeyboard()
        {
            if (CurrentFocus != null)
            {
                InputMethodManager imm = (InputMethodManager)GetSystemService(InputMethodService);
                imm.HideSoftInputFromWindow(CurrentFocus.WindowToken, HideSoftInputFlags.None);
            }
        }

        protected Intent GetIntentWithoutHistory(Context context, Type type)
        {
            Intent activityToStart = new Intent(context, type);
            activityToStart.AddFlags(ActivityFlags.NewTask);
            activityToStart.AddFlags(ActivityFlags.ClearTop);
            return activityToStart;
        }

        protected void SetTitleToAcionBar(string title, int icon, TextView actionBarTitleLabel)
        {
            if (!String.IsNullOrEmpty(title))
                actionBarTitleLabel.Text = title;
            if (icon != 0)
                actionBarTitleLabel.SetCompoundDrawablesWithIntrinsicBounds(null, null, GetDrawable(icon), null);
        }

        protected async Task<LoginResult> TryLoginOnStart()
        {
            var loginResult = await _authService.LoginAsync(_storageService.Email, _storageService.SecretKey, _storageService.ApiClientSecretKey);
            return loginResult;
        }
    }
}