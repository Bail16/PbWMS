﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using WMS.Mobile.Droid.Globals;
using WMS.Mobile.Http;
using WMS.Mobile.Extensions;
using System.Threading.Tasks;
using WMS.Mobile.Models;

namespace WMS.Mobile.Droid
{
    public class DashboardFragment : Fragment
    {
        Button _createTimeLogButton;
        Button _updateTimeLogButton;        

        private TextView _ovetimeTv;
        private TextView _worktimeTv;
        private TextView _workStatusLbl;
        private TextView _dateLbl;
        TimeLogService _timeLogService;
        OverWorkTimeService _overWorkTimeService;
        private Task<ActualDateTimeResult> _actualDateTimePromise;
        StorageService _storageService;
        private TextView _timeLbl;

        public DashboardFragment()
        {
            _timeLogService = new TimeLogService();
            _overWorkTimeService = new OverWorkTimeService();
            _actualDateTimePromise = _timeLogService.GetActualDateTime();
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _storageService = new StorageService(Activity.BaseContext);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.DashboardLayout, container, false);
            view.SetBackgroundColor(Color.White);

            GetResources(view);
            SetDateTime();
            GetOverTime();
            GetWorkTime();
            SetButtonStates();

            _createTimeLogButton.Click += _createTimeLogButton_Click;
            _updateTimeLogButton.Click += _updateTimeLogButton_Click;

            return view;
        }

        private async void CheckActiveTimeLog()
        {
            if (_storageService.ActiveTimeLogId.HasValue)
            {
                var timeLogResult = _timeLogService.GetTimeLog(_storageService.ActiveTimeLogId.Value);
                var timeLog = await timeLogResult;
                switch (timeLog.State)
                {
                    case HttpDefaultCodes.ok:
                        if (timeLog.LeaveTime != null)                        
                            _storageService.ActiveTimeLogId = null;                        
                        break;
                    case HttpDefaultCodes.error:
                        _storageService.ActiveTimeLogId = null;
                        break;
                    default:
                        break;
                }                               
            }
        }

        private async void SetDateTime()
        {
            var actualDateTimeResult = await _actualDateTimePromise;
            var actualDateTime = actualDateTimeResult.ActualDateTime;
            _dateLbl.Text = actualDateTime.ToDateString();
            _timeLbl.Text = actualDateTime.ToHourString();            
        }

        private async void _updateTimeLogButton_Click(object sender, EventArgs e)
        {
            var timeLogId = _storageService.ActiveTimeLogId;
            if (timeLogId != null)
            {
                var updateTimeLogREsult = await _timeLogService.StopWork(timeLogId.Value);
                switch (updateTimeLogREsult.State)
                {
                    case Models.HttpDefaultCodes.ok:
                        _storageService.ActiveTimeLogId = null;
                        Toast.MakeText(Context, "Success", ToastLength.Long).Show();
                        SetLoginButtonVisible(true);
                        break;
                    case Models.HttpDefaultCodes.error:
                        Toast.MakeText(Context, "Error", ToastLength.Long).Show();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                SetLoginButtonVisible(true);
            }
        }

        private async void _createTimeLogButton_Click(object sender, EventArgs e)
        {
            var userId = _storageService.UserId;
            if (userId != null)
            {
                var timeLogResult = await _timeLogService.StartWork(userId.Value);
                switch (timeLogResult.State)
                {
                    case Models.HttpDefaultCodes.ok:
                        _storageService.ActiveTimeLogId = timeLogResult.TimeLogId;
                        Toast.MakeText(Context, "Success", ToastLength.Short).Show();
                        SetLoginButtonVisible(false);
                        break;
                    case Models.HttpDefaultCodes.error:
                        Toast.MakeText(Context, "Error", ToastLength.Long).Show();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                SetLoginButtonVisible(false);
                //Activity.StartActivity(typeof(LoginActivity));
            }
        }



        private static void SetBackgroundToView(View view)
        {
            var linearLayout = view.FindViewById<LinearLayout>(Resource.Id.dashboard_mainLayout);
            linearLayout.SetBackgroundResource(Resource.Drawable.appBar_gradient);
        }

        private void SetButtonStates()
        {
            if (_storageService.ActiveTimeLogId != null)
            {
                SetLoginButtonVisible(false);
            }
            else
            {
                SetLoginButtonVisible(true);
            }
        }

        private void GetResources(View view)
        {
            _createTimeLogButton = view.FindViewById<Button>(Resource.Id.btn_startWork);
            _updateTimeLogButton = view.FindViewById<Button>(Resource.Id.btn_stopWork);
            _ovetimeTv = view.FindViewById<TextView>(Resource.Id.tv_overtime);
            _worktimeTv = view.FindViewById<TextView>(Resource.Id.tv_workTime);
            _workStatusLbl = view.FindViewById<TextView>(Resource.Id.lbl_workstatus);
            _dateLbl = view.FindViewById<TextView>(Resource.Id.tv_date);
            _timeLbl = view.FindViewById<TextView>(Resource.Id.tv_time);
        }

        private void SetLoginButtonVisible(bool isVisible)
        {
            if (isVisible)
            {
                _workStatusLbl.Text = GetString(Resource.String.dashboard_startWorkLbl);
                _createTimeLogButton.Visibility = ViewStates.Visible;
                _updateTimeLogButton.Visibility = ViewStates.Gone;
            }
            else
            {
                _workStatusLbl.Text = GetString(Resource.String.dashboard_stopWorkLbl);
                _createTimeLogButton.Visibility = ViewStates.Gone;
                _updateTimeLogButton.Visibility = ViewStates.Visible;
            }
        }

        private async void GetOverTime()
        {
            if (_storageService.UserId != null)
            {
                var overTimeResult = await _overWorkTimeService.GetOverTime(_storageService.UserId.Value);
                switch (overTimeResult.State)
                {
                    case Models.HttpDefaultCodes.ok:
                        _ovetimeTv.Text = overTimeResult.OverTime.ToWorkOverTimeString();
                        break;
                    case Models.HttpDefaultCodes.error:
                        Toast.MakeText(Context, "Error while loading Overtime", ToastLength.Long).Show();
                        break;
                    default:
                        break;
                }
            }
        }

        private async void GetWorkTime()
        {
            if (_storageService.UserId != null)
            {
                var workTimeResult = await _overWorkTimeService.GetWorkTime(_storageService.UserId.Value);
                switch (workTimeResult.State)
                {
                    case Models.HttpDefaultCodes.ok:
                        _worktimeTv.Text = workTimeResult.WorkTime.ToWorkOverTimeString();
                        break;
                    case Models.HttpDefaultCodes.error:
                        Toast.MakeText(Context, "Error while loading Worktime", ToastLength.Long).Show();
                        break;
                    default:
                        break;
                }
            }
        }
        
    }
}