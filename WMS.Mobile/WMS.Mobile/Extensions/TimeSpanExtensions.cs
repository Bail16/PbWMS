﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Extensions
{
    public static class TimeSpanExtensions
    {
        public static string ToWorkOverTimeString(this TimeSpan time)
        {
            return (int) time.TotalHours + "h " + time.ToString(@"mm") + "m";
        }
            
        public static string ToHourString(this TimeSpan time)
        {
            return time.ToString(@"hh\:mm") ?? "-";
        }
    }
}
