﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToDatePickerString(this DateTime date)
        {
            CultureInfo eng = new CultureInfo("en-US");
            return date.ToString("ddd", eng) + "., " + date.Day + '.' + date.Month + '.' + date.Year.ToString().Remove(0, 2);
        }

        public static string ToDateString(this DateTime date)
        {
            return date.ToString("dd.MM.yy");
        }

        public static string ToHourString(this DateTime date)
        {
            return date.ToString(@"HH:mm") ?? "-";
        }
        public static string ToHourString(this DateTime? date)
        {
            if(date != null)            
                return date.Value.ToString(@"HH:mm");            
            else            
                return "-";            
        }

    }
}
