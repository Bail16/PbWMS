﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Http
{
    public class UrlBuilder
    {
        public string _http { get; private set; }
        public string _origin { get; private set; }
        public string _timeLogCtrl { get; private set; }
        public string _overtimeCtrl { get; private set; }
        public string _worktimeCtrl { get; private set; }
        public string _loginCtrl { get; private set; }
        public string _dateTimeCtrl { get; private set; }
        public string _loginMth { get; private set; }
        public string _logoutMth { get; private set; }
        public string _roomCtrl { get; private set; }
        public string _reservationCtrl { get; private set; }

        public UrlBuilder()
        {
            _http = "http://";
            //_origin = "10.0.2.2:55987";
            _origin = "wmspb.azurewebsites.net";
            _timeLogCtrl = "TimeLog";
            _overtimeCtrl= "Overtime";
            _worktimeCtrl = "Worktime";
            _loginCtrl = "Login";
            _dateTimeCtrl = "Time";
            _roomCtrl = "Rooms";
            _reservationCtrl = "Reservations";
            
        }

        public string GetLoginUrl()
        {
            return String.Format("{0}{1}/{2}/", _http, _origin, _loginCtrl);
        }
               

        public string GetCreateTimeLogUrl()
        {
            return String.Format("{0}{1}/{2}/", _http, _origin, _timeLogCtrl);
        }

        public string GetUpdateTimeLogUrl(Guid id)
        {
            return String.Format("{0}{1}/{2}/{3}", _http, _origin, _timeLogCtrl, id);
        }

        public string GetOverTimeUrl(Guid userId, DateTime startDate, DateTime endDate)
        {
            return String.Format("{0}{1}/{2}/{3}/{4}/{5}", _http, _origin, _overtimeCtrl, userId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
        }

        public string GetWorkTimeUrl(Guid userId, DateTime startDate, DateTime endDate)
        {
            return String.Format("{0}{1}/{2}/{3}/{4}/{5}", _http, _origin, _worktimeCtrl, userId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
        }

        public string GetTimeLogUrl(Guid timeLogId)
        {
            return String.Format("{0}{1}/{2}/{3}", _http, _origin, _timeLogCtrl, timeLogId);
        }

        public string GetTimeLogsUrl(Guid userId, DateTime startDate, DateTime endDate)
        {
            return String.Format("{0}{1}/{2}/{3}/{4}/{5}", _http, _origin, _timeLogCtrl, userId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
        }

        public string GetActualDateTimeUrl()
        {
            return String.Format("{0}{1}/{2}", _http, _origin, _dateTimeCtrl);
        }

        public string GetRoomsUrl()
        {
            return String.Format("{0}{1}/{2}", _http, _origin, _roomCtrl);
        }

        public string GetRoomReseravtionsUrl(Guid roomId, DateTime date)
        {
            return String.Format("{0}{1}/{2}/{3}/{4}", _http, _origin, _reservationCtrl, roomId, date.ToString("yyyy-MM-dd"));
        }

        public string GetCreateRoomReservationUrl()
        {
            return String.Format("{0}{1}/{2}", _http, _origin, _reservationCtrl);
        }
    }
}
