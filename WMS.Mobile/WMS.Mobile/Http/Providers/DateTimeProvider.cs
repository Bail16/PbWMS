﻿using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Http.Providers
{
    public class DateTimeProvider : MyHttpProvider
    {
        public async Task<string> GetActualDateTime()
        {
            var getDateTimeUrl = _urlBuilder.GetActualDateTimeUrl();
            var response = await getDateTimeUrl
                .WithTimeout(TimeoutInSeconds)
                .GetAsync()
                .ReceiveString();

            return response;
        }
    }
}
