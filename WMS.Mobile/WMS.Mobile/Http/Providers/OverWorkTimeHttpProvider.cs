﻿using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMS.Mobile.Models;

namespace WMS.Mobile.Http
{
    public class OverWorkTimeHttpProvider : MyHttpProvider
    {
        
        public async Task<string> GetOverTime(GetOverWorkTimeDto model)
        {
            var urlGetOverTime = _urlBuilder.GetOverTimeUrl(model.UserId, model.StartDate, model.EndDate);
            var response = await urlGetOverTime
                .WithTimeout(TimeoutInSeconds)
                .WithHeader(ApiClientSecretKeyName, model.ApiClientSecretKey)

                .GetAsync()
                .ReceiveString();

            return response;
        }

        public async Task<string> GetWorkTime(GetOverWorkTimeDto model)
        {
            var urlGetWorkTime = _urlBuilder.GetWorkTimeUrl(model.UserId, model.StartDate, model.EndDate);
            var response = await urlGetWorkTime
                .WithTimeout(TimeoutInSeconds)
                .WithHeader(ApiClientSecretKeyName, model.ApiClientSecretKey)
                .GetAsync()
                .ReceiveString();

            return response;
        }
    }
}
