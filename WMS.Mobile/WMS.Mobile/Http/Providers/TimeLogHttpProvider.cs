﻿using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMS.Mobile.Models;

namespace WMS.Mobile.Http
{
    public class TimeLogHttpProvider : MyHttpProvider
    {
       
        public async Task<string> CreateTimeLog(CreateTimeLogDto model)
        {
            var urlLogin = _urlBuilder.GetCreateTimeLogUrl();
            var response = await urlLogin
                .WithTimeout(TimeoutInSeconds)
                .WithHeader(ApiClientSecretKeyName, model.ApiClientSecretKey)
                .PostJsonAsync(model)
                .ReceiveString();

            return response;
        }

        public async Task<string> UpdateTimeLog(UpdateTimeLogDto model)
        {
            var urlUpdate = _urlBuilder.GetUpdateTimeLogUrl(model.TimeLogId);
            var response = await urlUpdate
                .WithTimeout(TimeoutInSeconds)
                .WithHeader(ApiClientSecretKeyName, model.ApiClientSecretKey)
                .PutJsonAsync(model)
                .ReceiveString();

            return response;
        }

        public async Task<string> GetTimeLogs(GetTimeLogsDTO model)
        {
            var urlGetTimeLogs = _urlBuilder.GetTimeLogsUrl(model.UserId, model.StartDate, model.EndDate);
            var response = await urlGetTimeLogs
                .WithTimeout(TimeoutInSeconds)
                .WithHeader(ApiClientSecretKeyName, model.ApiClientSecretKey)
                .GetAsync()
                .ReceiveString();

            return response;
        }

        public async Task<string> GetTimeLog(GetTimeLogDto model)
        {
            var urlGetTimeLog = _urlBuilder.GetTimeLogUrl(model.TimeLogId);
            var response = await urlGetTimeLog
                .WithTimeout(TimeoutInSeconds)
                .WithHeader(ApiClientSecretKeyName, model.ApiClientSecretKey)
                .GetAsync()
                .ReceiveString();

            return response;
        }

    }
}
