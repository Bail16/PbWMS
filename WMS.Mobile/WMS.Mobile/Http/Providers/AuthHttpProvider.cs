﻿using Flurl.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WMS.Mobile.Models;

namespace WMS.Mobile.Http
{
    public class AuthHttpProvider : MyHttpProvider
    {              
        public async Task<string> LoginAsync(LoginDto model)
        {
            var urlLogin = _urlBuilder.GetLoginUrl();
            var response = await urlLogin
                .WithTimeout(TimeoutInSeconds)
                .WithHeader(ApiClientSecretKeyName, model.ApiClientSecretKey)
                .PostJsonAsync(model)
                .ReceiveString();

            return response;
        }
    }
}
