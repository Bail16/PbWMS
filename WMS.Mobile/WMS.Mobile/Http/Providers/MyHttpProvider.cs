﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Http
{
    public class MyHttpProvider
    {
        protected UrlBuilder _urlBuilder { get; set; }

        public MyHttpProvider()
        {
            _urlBuilder = new UrlBuilder();
            TimeoutInSeconds = 10;
            ApiClientSecretKeyName = "apiClientSecretKey";
        }

        public int TimeoutInSeconds { get; set; }
        public string ApiClientSecretKeyName { get; set; }
    }
}
