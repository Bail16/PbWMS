﻿using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMS.Mobile.Models;

namespace WMS.Mobile.Http.Providers
{
    public class RoomHttpProvider : MyHttpProvider
    {
        public async Task<string> GetAllRooms(GetRoomsDto model)
        {
            var getAllRoomsUrl = _urlBuilder.GetRoomsUrl();
            var response = await getAllRoomsUrl
                .WithTimeout(TimeoutInSeconds)
                .WithHeader(ApiClientSecretKeyName, model.ApiClientSecretKey)
                .GetAsync()
                .ReceiveString();

            return response;
        }

        public async Task<string> GetRoomReservations(GetRoomReservationsDto model)
        {
            var getRoomReservationsUrl = _urlBuilder.GetRoomReseravtionsUrl(model.RoomId, model.Date);
            var response = await getRoomReservationsUrl
                .WithTimeout(TimeoutInSeconds)
                .WithHeader(ApiClientSecretKeyName, model.ApiClientSecretKey)
                .GetAsync()
                .ReceiveString();

            return response;
        }

        public async Task<string> CreateRoomReservation(CreateRoomReservationDto model)
        {
            var createReservationUrl = _urlBuilder.GetCreateRoomReservationUrl();
            var response = await createReservationUrl
                .WithTimeout(TimeoutInSeconds)
                .WithHeader(ApiClientSecretKeyName, model.ApiClientSecretKey)
                .PostJsonAsync(model)
                .ReceiveString();

            return response;
        }
    }
}
