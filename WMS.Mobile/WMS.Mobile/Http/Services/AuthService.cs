﻿using Flurl.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WMS.Mobile.Mappers;
using WMS.Mobile.Models;

namespace WMS.Mobile.Http
{
    public class AuthService : MyService
    {
        private AuthHttpProvider _authProvider;
        private AuthMapper _authMapper;

        public AuthService()
        {
            _authProvider = new AuthHttpProvider();
            _authMapper = new AuthMapper();
        }

        public async Task<LoginResult> LoginAsync(string email, string secretKey, string apiClientSecret)
        {
            try
            {
                var loginModel = _authMapper.MapToLoginModel(email, secretKey, apiClientSecret);
                var result = await _authProvider.LoginAsync(loginModel);
                var resultDTO = JsonConvert.DeserializeObject<LoginResultDTO>(result);
                if (resultDTO.IsSuccess)                
                    return new LoginResult(HttpLoginCodes.ok, resultDTO.UserId, resultDTO.Name, resultDTO.Surname);                
                else
                    return new LoginResult(HttpLoginCodes.badEmailOrCode);
            }
            catch(Exception e)
            {
                return new LoginResult(HttpLoginCodes.connectionError);
            }
        }
    }
}
