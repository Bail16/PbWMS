﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMS.Mobile.Models;

namespace WMS.Mobile.Http
{
    public class TimeLogService : MyService
    {
        TimeLogHttpProvider _timeLogProvider;
        public TimeLogService()
        {
            _timeLogProvider = new TimeLogHttpProvider();
        }
        public async Task<CreateTimeLogResult> StartWork(Guid userId)
        {
            try
            {
                var model = new CreateTimeLogDto(userId, TimeLogType.Regular);
                var response = await _timeLogProvider.CreateTimeLog(model);
                var result = JsonConvert.DeserializeObject<CreateTimeLogResultDto>(response);
                if (result.TimeLogId.HasValue)
                {
                    return new CreateTimeLogResult(result.TimeLogId.Value, HttpDefaultCodes.ok);
                }
                else
                {
                    return new CreateTimeLogResult(HttpDefaultCodes.error);
                }
            }
            catch (Exception e)
            {
                return new CreateTimeLogResult(HttpDefaultCodes.error);
            }
        }

        public async Task<UpdateTimeLogResult> StopWork(Guid timeLogId)
        {
            try
            {
                var model = new UpdateTimeLogDto(timeLogId);
                var response = await _timeLogProvider.UpdateTimeLog(model);
                var result = false;
                if (bool.TryParse(response, out result))
                {
                    return new UpdateTimeLogResult(HttpDefaultCodes.ok);
                }
                return new UpdateTimeLogResult(HttpDefaultCodes.error);
            }
            catch (Exception e)
            {
                return new UpdateTimeLogResult(HttpDefaultCodes.error);
            }

        }

        public async Task<GetTimeLogsResult> GetTimeLogs(Guid userId, DateTime startDate, DateTime endDate, DateTime now)
        {
            try
            {
                var model = new GetTimeLogsDTO(userId, startDate, endDate);
                var response = await _timeLogProvider.GetTimeLogs(model);
                var result = JsonConvert.DeserializeObject<GetTimeLogsResultDTO>(response);
                if (result.IsSuccess)
                {
                    var timeLogs = new List<GetTimeLogResult>();
                    foreach (var timeLog in result.TimeLogs)
                    {
                        TimeSpan workTime = new TimeSpan();
                        DateTime? leaveTime = null;
                        if (timeLog.LeaveTime != null)
                        {
                            leaveTime = timeLog.LeaveTime.Value;
                            workTime = timeLog.LeaveTime.Value.TimeOfDay - timeLog.EntryTime.TimeOfDay;
                        }
                        else
                        {
                            workTime = now.TimeOfDay - timeLog.EntryTime.TimeOfDay;
                        }
                        timeLogs.Add(new GetTimeLogResult
                        {
                            EntryTime = timeLog.EntryTime,
                            LeaveTime = leaveTime,
                            WorkTime = workTime,
                            TimeLogEntryType = timeLog.TimeLogEntryType,
                            TypeCamelCaseName = timeLog.TypeCamelCaseName,
                            IsApproved = timeLog.IsApproved
                        });
                    }
                    return new GetTimeLogsResult(HttpDefaultCodes.ok, timeLogs);
                }
                return new GetTimeLogsResult(HttpDefaultCodes.error);
            }
            catch (Exception e)
            {
                return new GetTimeLogsResult(HttpDefaultCodes.error);
            }
        }

        public async Task<GetSingleTimeLogResult> GetTimeLog(Guid timeLogId)
        {
            try
            {
                var model = new GetTimeLogDto(timeLogId);
                var response = await _timeLogProvider.GetTimeLog(model);
                var result = JsonConvert.DeserializeObject<GetTimeLogResultDTO>(response);

                if (result != null)
                {
                    return new GetSingleTimeLogResult
                    {
                        EntryTime = result.EntryTime,
                        LeaveTime = result.LeaveTime,
                        TimeLogEntryType = result.TimeLogEntryType,
                        TypeCamelCaseName = result.TypeCamelCaseName,
                        IsApproved = result.IsApproved
                    };
                }
                return new GetSingleTimeLogResult(HttpDefaultCodes.ok);
            }
            catch (Exception e)
            {
                return new GetSingleTimeLogResult(HttpDefaultCodes.error);
            }
        }
    }
}
