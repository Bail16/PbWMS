﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMS.Mobile.Models;

namespace WMS.Mobile.Http
{
    public class OverWorkTimeService : MyService
    {
        OverWorkTimeHttpProvider _overWorkTimeProvider;
        public OverWorkTimeService()
        {
             _overWorkTimeProvider = new OverWorkTimeHttpProvider();
        }

        public async Task<GetOverTimeResult> GetOverTime(Guid userId)
        {
            try
            {
                var now = DateTime.Now;
                var model = new GetOverWorkTimeDto(userId, new DateTime(now.Year, now.Month, 1), now);
                var response = await _overWorkTimeProvider.GetOverTime(model);
                var result = JsonConvert.DeserializeObject<GetOverTimeResultDto>(response);
                if(result.IsSuccess)
                {
                    return new GetOverTimeResult(HttpDefaultCodes.ok, TimeSpan.Parse(result.OverTime));
                }
                else
                {
                    return new GetOverTimeResult(HttpDefaultCodes.error);
                }
            }
            catch (Exception e)
            {
                return new GetOverTimeResult(HttpDefaultCodes.error);
            }
        }

        public async Task<GetWorkTimeResult> GetWorkTime(Guid userId)
        {
            try
            {
                var now = DateTime.Now;
                var model = new GetOverWorkTimeDto(userId, new DateTime(now.Year, now.Month, 1), now);
                var response = await _overWorkTimeProvider.GetWorkTime(model);
                var result = JsonConvert.DeserializeObject<GetWorkTimeResultDto>(response);
                if (result.IsSuccess)
                {
                    return new GetWorkTimeResult(HttpDefaultCodes.ok, TimeSpan.Parse(result.WorkTime));
                }
                else
                {
                    return new GetWorkTimeResult(HttpDefaultCodes.error);
                }
            }
            catch (Exception e)
            {
                return new GetWorkTimeResult(HttpDefaultCodes.error);
            }
        }
    }
}
