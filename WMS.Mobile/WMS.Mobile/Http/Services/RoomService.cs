﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMS.Mobile.Http.Providers;
using WMS.Mobile.Models;

namespace WMS.Mobile.Http.Services
{
    public class RoomService : MyService
    {
        private RoomHttpProvider _roomHttpProvider;

        public RoomService()
        {
            _roomHttpProvider = new RoomHttpProvider();
        }
        public async Task<GetAllRoomsResult> GetAllRooms()
        {
            try
            {
                var getRoomsModel = new GetRoomsDto();
                var resultJson = await _roomHttpProvider.GetAllRooms(getRoomsModel);
                var result = JsonConvert.DeserializeObject<GetAllRoomsResultDto>(resultJson);
                if (result.IsSuccess)
                {
                    var rooms = new List<GetRoomResult>();
                    foreach (var room in result.Rooms)
                    {
                        rooms.Add(new GetRoomResult
                        {
                            RoomName = room.Name,
                            RoomId = room.Id
                        });
                    }
                    return new GetAllRoomsResult(HttpDefaultCodes.ok, rooms);
                }
                return new GetAllRoomsResult(HttpDefaultCodes.error);
            }
            catch (Exception e)
            {
                return new GetAllRoomsResult(HttpDefaultCodes.error);
            }
        }

        public async Task<GetRoomReservationsResult> GetRoomReservations(Guid roomId, DateTime date)
        {
            try
            {
                var getReservationsModel = new GetRoomReservationsDto { Date = date, RoomId = roomId };
                var resultJson = await _roomHttpProvider.GetRoomReservations(getReservationsModel);
                var result = JsonConvert.DeserializeObject<GetRoomReservationsResultDto>(resultJson);
                if (result.IsSuccess)
                {
                    var reservations = new List<GetRoomReservationResult>();
                    foreach (var reservation in result.Reservations)
                    {
                        reservations.Add(new GetRoomReservationResult
                        {
                            TimeFrom = TimeSpan.Parse(reservation.TimeFrom),
                            TimeTo = TimeSpan.Parse(reservation.TimeTo),
                            UserSurname = reservation.UserSurname,
                            UserName = reservation.UserName,
                            ReservationDoneAt = reservation.ReservationDoneAt
                        });
                    }
                    return new GetRoomReservationsResult(HttpDefaultCodes.ok, reservations);
                }
                return new GetRoomReservationsResult(HttpDefaultCodes.error);
            }
            catch (Exception e)
            {
                return new GetRoomReservationsResult(HttpDefaultCodes.error);
            }
        }

        public async Task<CreateRoomReservationResult> CreateRoomReservation(Guid roomId, DateTime date, TimeSpan timeFrom, TimeSpan timeTo)
        {
            try
            {
                var model = new CreateRoomReservationDto { Date = date, TimeTo = timeTo, TimeFrom = timeFrom, RoomId = roomId };
                var response = await _roomHttpProvider.CreateRoomReservation(model);
                var result = JsonConvert.DeserializeObject<CreateRoomReservationResultDto>(response);
                if (result.IsSuccess)
                {
                    return new CreateRoomReservationResult(HttpDefaultCodes.ok);
                }
                else
                {
                    return new CreateRoomReservationResult(HttpDefaultCodes.error);
                }
            }
            catch (Exception e)
            {
                return new CreateRoomReservationResult(HttpDefaultCodes.error);
            }
        }
    }
}
