﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMS.Mobile.Http.Providers;
using WMS.Mobile.Models;

namespace WMS.Mobile.Http
{
    public class MyService
    {
        private DateTimeProvider _dateTimeProvider;
        public MyService()
        {
            _dateTimeProvider = new DateTimeProvider();
        }
        public async Task<ActualDateTimeResult> GetActualDateTime()
        {
            try
            {
                var result = await _dateTimeProvider.GetActualDateTime();
                var resultDTO = JsonConvert.DeserializeObject<ActualDateTimeResultDTO>(result);
                if (resultDTO.IsSuccess)
                    return new ActualDateTimeResult(resultDTO.ActualDate, HttpLoginCodes.ok);
                else
                    return new ActualDateTimeResult(HttpLoginCodes.connectionError);
            }
            catch (Exception e)
            {
                return new ActualDateTimeResult(HttpLoginCodes.connectionError);
            }
        }
    }
}
