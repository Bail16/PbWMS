﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WMS.Mobile
{
    public class ValidationComponent
    {
        private Regex _emailRegex;
        private const int _secretKeyLength = 6; 

        public ValidationComponent()
        {
            _emailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        }

        public bool ValidateEmail(string email)
        {
            Match match = _emailRegex.Match(email);
            if (match.Success)
            {
                return true;
            }
            else return false;
        }

        public bool ValidateSecretKey(string secretkey)
        {
            if(secretkey.Length == _secretKeyLength)
            {
                return true;
            }
            return false;
        }

    }
}
