﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMS.Mobile.Models;

namespace WMS.Mobile.Mappers
{
    public class AuthMapper
    {
        public LoginDto MapToLoginModel(string email, string secretKey, string apiClientSecret)
        {
            return new LoginDto
            {
                Email = email,
                UserSecretKey = secretKey,
                ApiClientSecretKey = apiClientSecret
            };
        }
    }
}
