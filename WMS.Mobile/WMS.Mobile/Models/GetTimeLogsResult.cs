﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class GetTimeLogsResult
    {
        public GetTimeLogsResult()
        {
            TimeLogs = new List<GetTimeLogResult>();
        }
        public GetTimeLogsResult(HttpDefaultCodes state)
        {
            State = state;
            TimeLogs = new List<GetTimeLogResult>();
        }

        public GetTimeLogsResult(HttpDefaultCodes state, IEnumerable<GetTimeLogResult> timeLogs): this(state)
        {
            TimeLogs = timeLogs.ToList();
        }

        public HttpDefaultCodes State { get; set; }
        public List<GetTimeLogResult> TimeLogs { get; set; }
        public TimeSpan DayWorkTime { get
            {
                var workTime = new TimeSpan();
                foreach (var timeLog in TimeLogs)
                {
                    workTime = workTime.Add(timeLog.WorkTime);
                }
                return workTime;
            }
        }
    }
}
