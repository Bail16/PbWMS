﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class LoginDto : ApiRequestModel
    {
        public string Email { get; set; }
        public string UserSecretKey { get; set; }
    }
}
