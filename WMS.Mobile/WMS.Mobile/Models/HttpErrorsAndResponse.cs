﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public enum HttpLoginCodes
    {
        ok = 0,
        badEmailOrCode = 1,
        connectionError = 2
    }

    public enum HttpDefaultCodes
    {
        ok = 0,
        error = 1
    }
}
