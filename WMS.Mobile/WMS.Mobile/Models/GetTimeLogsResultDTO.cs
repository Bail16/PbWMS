﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class GetTimeLogsResultDTO
    {
        public GetTimeLogsResultDTO()
        {
            TimeLogs = new List<GetTimeLogResultDTO>();
        }
        public bool IsSuccess { get; set; }
        public List<GetTimeLogResultDTO> TimeLogs { get; set; }
    }
}
