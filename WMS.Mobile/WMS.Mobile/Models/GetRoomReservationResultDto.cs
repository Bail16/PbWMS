﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class GetRoomReservationResultDto
    {
        public DateTime Date { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public DateTime ReservationDoneAt { get; set; }
        public string UserName { get; set; }
        public string UserSurname { get; set; }
    }
}
