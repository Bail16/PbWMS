﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class CreateTimeLogResult
    {
        public CreateTimeLogResult(HttpDefaultCodes state)
        {
            State = state;
        }

        public CreateTimeLogResult(Guid timeLogId, HttpDefaultCodes state)
        {
            TimeLogId = timeLogId;
            State = state;
        }
        public Guid TimeLogId { get; set; }
        public HttpDefaultCodes State { get; set; }
    }
}
