﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class GetRoomReservationsResult
    {
        public GetRoomReservationsResult()
        {
            Reservations = new List<GetRoomReservationResult>();
        }
        public GetRoomReservationsResult(HttpDefaultCodes state)
        {
            State = state;
            Reservations = new List<GetRoomReservationResult>();
        }

        public GetRoomReservationsResult(HttpDefaultCodes state, IEnumerable<GetRoomReservationResult> reservations): this(state)
        {
            Reservations = reservations.ToList();
        }

        public HttpDefaultCodes State { get; set; }
        public List<GetRoomReservationResult> Reservations { get; set; }    
    }
}
