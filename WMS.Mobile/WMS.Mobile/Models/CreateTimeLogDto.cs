﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class CreateTimeLogDto : ApiRequestModel
    {
        public CreateTimeLogDto(Guid userId, TimeLogType timeLogEntryType)
        {
            UserId = userId;
            TimeLogEntryType = timeLogEntryType;
        }
        public Guid UserId { get; set; }        

        public TimeLogType TimeLogEntryType { get; set; }
    }
}
