﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class GetAllRoomsResult
    {
        public GetAllRoomsResult()
        {
            Rooms = new List<GetRoomResult>();
        }
        public GetAllRoomsResult(HttpDefaultCodes state)
        {
            State = state;
            Rooms = new List<GetRoomResult>();
        }

        public GetAllRoomsResult(HttpDefaultCodes state, IEnumerable<GetRoomResult> rooms): this(state)
        {
            Rooms = rooms.ToList();
        }

        public HttpDefaultCodes State { get; set; }
        public List<GetRoomResult> Rooms { get; set; }     
    }
}
