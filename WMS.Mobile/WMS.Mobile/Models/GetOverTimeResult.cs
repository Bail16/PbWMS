﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class GetOverTimeResult
    {
        public GetOverTimeResult(HttpDefaultCodes state)
        {
            State = state;
        }
        public GetOverTimeResult(HttpDefaultCodes state, TimeSpan overTime)
        {
            State = state;
            OverTime = overTime;
        }

        public HttpDefaultCodes State { get; set; }
        public TimeSpan OverTime { get; set; }
    }
}
