﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class UpdateTimeLogDto : ApiRequestModel
    {
        public UpdateTimeLogDto(Guid timeLogId)
        {
            TimeLogId = timeLogId;
        }

        public Guid TimeLogId { get; set; }
    }
}
