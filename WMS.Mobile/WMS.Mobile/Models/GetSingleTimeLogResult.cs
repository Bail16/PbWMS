﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class GetSingleTimeLogResult
    {
        public GetSingleTimeLogResult(HttpDefaultCodes state)
        {
            State = state;
        }
        public GetSingleTimeLogResult()
        {
        }

        public DateTime EntryTime { get; set; }
        public DateTime? LeaveTime { get; set; }

        public bool IsApproved { get; set; }
        public TimeLogType TimeLogEntryType { get; set; }
        public string TypeCamelCaseName { get; set; }

        public HttpDefaultCodes State { get; set; }
    }
}
