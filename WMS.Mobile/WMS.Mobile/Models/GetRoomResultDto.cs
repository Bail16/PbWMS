﻿using System;

namespace WMS.Mobile.Models
{
    public class GetRoomResultDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}