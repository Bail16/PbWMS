﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class ActualDateTimeResultDTO
    {
        public DateTime ActualDate { get; set; }
        public bool IsSuccess { get; set; }
    }
}
