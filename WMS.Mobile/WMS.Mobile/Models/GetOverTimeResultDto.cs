﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class GetOverTimeResultDto
    {
        public string OverTime { get; set; }
        public bool IsSuccess { get; set; }
    }
}
