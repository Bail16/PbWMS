﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class GetWorkTimeResultDto
    {
        public string WorkTime { get; set; }
        public bool IsSuccess { get; set; }
    }
}
