﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class GetRoomReservationsResultDto
    {
        public GetRoomReservationsResultDto()
        {
            Reservations = new List<GetRoomReservationResultDto>();
        }
        public bool IsSuccess { get; set; }
        public List<GetRoomReservationResultDto> Reservations { get; set; }
    }
}
