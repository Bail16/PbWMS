﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class GetRoomReservationResult
    {
        public string UserName { get; set; }
        public string UserSurname { get; set; }

        public TimeSpan TimeFrom { get; set; }  
        public TimeSpan TimeTo { get; set; }

        public DateTime ReservationDoneAt { get; set; } 
    }
}
