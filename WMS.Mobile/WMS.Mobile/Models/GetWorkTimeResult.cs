﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class GetWorkTimeResult
    {
        public GetWorkTimeResult(HttpDefaultCodes state)
        {
            State = state;
        }
        public GetWorkTimeResult(HttpDefaultCodes state, TimeSpan workTime)
        {
            State = state;
            WorkTime = workTime;
        }

        public HttpDefaultCodes State { get; set; }
        public TimeSpan WorkTime { get; set; }
    }
}
