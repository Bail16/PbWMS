﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class UpdateTimeLogResult
    {
        public UpdateTimeLogResult(HttpDefaultCodes state)
        {
            State = state;
        }
        public HttpDefaultCodes State { get; set; }
    }
}
