﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class ActualDateTimeResult
    {
        public ActualDateTimeResult(HttpLoginCodes state)
        {
            State = state;
        }
        public ActualDateTimeResult(DateTime actualDateTime, HttpLoginCodes state) : this(state)
        {
            ActualDateTime = actualDateTime;
        }
        public DateTime ActualDateTime { get; set; }
        public HttpLoginCodes State { get; set; }
    }
}
