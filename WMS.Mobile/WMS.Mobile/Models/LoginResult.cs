﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMS.Mobile.Http;

namespace WMS.Mobile.Models
{
    public class LoginResult
    {
        public LoginResult(HttpLoginCodes loginState)
        {
            State = loginState;
        }

        public LoginResult(HttpLoginCodes loginState, Guid userId, string name, string surname): this(loginState)
        {
            UserId = userId;
            Name = name;
            Surname = surname;
        }
        public Guid UserId { get; set; }
        public HttpLoginCodes State { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
