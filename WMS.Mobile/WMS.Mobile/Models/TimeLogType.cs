﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WMS.Mobile.Models
{
    public enum TimeLogType
    {
        Regular,
        DayOff,
        HalfDayOff,
        SickLeave,
        Delegation,
        ForgotToLogIn,
        ForgotToLogOut
    }
}