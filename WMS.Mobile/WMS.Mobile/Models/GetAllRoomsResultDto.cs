﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class GetAllRoomsResultDto
    {
        public bool IsSuccess { get; set; }

        public IEnumerable<GetRoomResultDto> Rooms {get;set;}
    }
}
