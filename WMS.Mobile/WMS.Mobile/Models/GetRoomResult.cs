﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class GetRoomResult
    {
        public string RoomName { get; set; }
        public Guid RoomId { get; set; }
    }
}
