﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class GetRoomReservationsDto : ApiRequestModel
    {
        public Guid RoomId { get; set; }
        public DateTime Date { get; set; }
    }
}
