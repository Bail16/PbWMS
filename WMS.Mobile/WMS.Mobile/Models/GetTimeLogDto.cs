﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class GetTimeLogDto : ApiRequestModel
    {
        public GetTimeLogDto(Guid timeLogId)
        {
            TimeLogId = timeLogId;
        }
        public Guid TimeLogId { get; set; }
    }
}
