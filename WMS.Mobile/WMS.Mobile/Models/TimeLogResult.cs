﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Mobile.Models
{
    public class TimeLogResult
    {
        public DateTime EntryTime { get; set; }
        public DateTime? LeaveTime { get; set; }

        public TimeSpan WorkTime { get; set; }

        public bool IsApproved { get; set; }
        public TimeLogType TimeLogEntryType { get; set; }
        public string TypeCamelCaseName { get; set; }
    }
}
