﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.DTO
{
    public class LoginDTO
    {
        public string Email { get; set; }
        public string UserSecretKey { get; set; }
    }
}