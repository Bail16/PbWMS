﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.DTO
{
    public class LoginResultDTO
    {
        public LoginResultDTO(bool isSuccess)
        {
            IsSuccess = IsSuccess;
            UserId = Guid.Empty;
        }

        public LoginResultDTO(bool isSuccess, Guid userId)
        {
            IsSuccess = isSuccess;
            UserId = userId;
        }
        public bool IsSuccess { get; set; }
        public Guid UserId { get; set; }
    }
}