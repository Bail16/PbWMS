﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WMS.Api.Enums;

namespace WMS.DTO
{
    public class GetTimeLogDTO
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid? UpdateUserId { get; set; }
                
        public DateTime EntryTime { get; set; }
        public DateTime? LeaveTime { get; set; }
        
        public bool IsApproved { get; set; }
        public TimeLogType TimeLogEntryType { get; set; }

        public Guid TimeLogEntryTypeId { get; set; }
        
        public Guid AppLogId { get; set; }
    }
}