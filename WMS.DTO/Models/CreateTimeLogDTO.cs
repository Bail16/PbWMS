﻿using System;

namespace WMS.DTO
{
    public class CreateTimeLogDTO
    {
        public Guid UserId { get; set; }

        public DateTime EntryTime { get; set; }        

        public TimeLogType TimeLogEntryType { get; set; }
    }
}