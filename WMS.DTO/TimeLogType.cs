﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.DTO
{
    public enum TimeLogType
    {
        Regular,
        DayOff,
        HalfDayOff,
        SickLeave,
        Delegation,
        ForgotToLogIn,
        ForgotToLogOut
    }
}