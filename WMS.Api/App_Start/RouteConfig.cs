﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace WMS.Api
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            MapOverWorkTimeRoutes(routes);
            MapLoginRoutes(routes);
            MapTimeLogRoutes(routes);
            MapRoomRoutes(routes);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }

        private static void MapRoomRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                 name: "GetAllRooms",
                 url: "Rooms",
                 defaults: new { controller = "Room", action = "GetAllRooms" },
                 constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "GET" }) }
              );

            routes.MapRoute(
              name: "GetAllRoomReservations",
              url: "Reservations/{roomId}/{date}",
              defaults: new { controller = "Room", action = "GetAllRoomReservations" },
              constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "GET" }) }
           );

            routes.MapRoute(
              name: "CreateRoomReservation",
              url: "Reservations",
              defaults: new { controller = "Room", action = "CreateRoomReservation" },
              constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "POST" }) }
           );
        }

        private static void MapTimeLogRoutes(RouteCollection routes)
        {
            routes.MapRoute(
             name: "GetTimeLogs",
             url: "TimeLog/{userId}/{startDate}/{endDate}",
             defaults: new { controller = "TimeLog", action = "GetTimeLogs" },
              constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "GET" }) }
             );

            routes.MapRoute(
               name: "DeleteTimeLog",
               url: "TimeLog/{id}",
               defaults: new { controller = "TimeLog", action = "DeleteTimeLog" },
               constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "DELETE" }) }
            );

            routes.MapRoute(
               name: "GetTimeLog",
               url: "TimeLog/{id}",
               defaults: new { controller = "TimeLog", action = "GetTimeLog" },
               constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "GET" }) }
            );


            routes.MapRoute(
                name: "CrateTimeLog",
                url: "TimeLog",
                defaults: new { controller = "TimeLog", action = "CreateTimeLog" },
                constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "POST" }) }
            );

            routes.MapRoute(
                name: "UpdateTimeLog",
                url: "TimeLog/{id}",
                defaults: new { controller = "TimeLog", action = "UpdateTimeLog" },
                constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "PUT" }) }
    );

        }
        private static void MapLoginRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                name: "Login",
                url: "Login",
                defaults: new { controller = "Login", action = "Login" },
                constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "POST" }) }
            );

        }
        private static void MapOverWorkTimeRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                name: "WorkTime",
                url: "Worktime/{userId}/{startDate}/{endDate}",
                defaults: new { controller = "OverWorkTime", action = "GetWorkTime" },
                constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "GET" }) }
            );

            routes.MapRoute(
              name: "OverTime",
              url: "Overtime/{userId}/{startDate}/{endDate}",
              defaults: new { controller = "OverWorkTime", action = "GetOverTime" },
              constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "GET" }) }
          );

        }
    }
}
