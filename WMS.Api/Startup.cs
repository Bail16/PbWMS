﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WMS.Api.Startup))]
namespace WMS.Api
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
