﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WMS.Api.DAL.DatabaseContext;
using WMS.Api.DAL.Models.ApiAuthModels;
using WMS.Api.Services;
using WMS.Api.ViewModels.ApiClientViewModels;

namespace WMS.Api.Controllers
{
    public class AdministrationController : Controller
    {
        private ApiClientService _apiClientService;

        public AdministrationController()
        {
            _apiClientService = new ApiClientService();
        }
        // GET: Administration
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddApiClient()
        {
            return View(new CreateApiClientViewModel());
        }

        [HttpPost]
        public ActionResult AddApiClient(CreateApiClientViewModel apiClientVM)
        {
            if (ModelState.IsValid)
            {
                var apiClientId = _apiClientService.CreateApiClient(apiClientVM);
                return RedirectToAction("GetApiClientSecretKey", new { id = apiClientId });
            }
            else return View(apiClientVM);
        }

        public ActionResult GetApiClientSecretKey(Guid id)
        {
            var secretKey = _apiClientService.GetSecretKey(id);
            return View(secretKey);
        }

        public ActionResult ShowAllApiClients()
        {
            var allApiClients = _apiClientService.GetAllApiClients();         
            return View(allApiClients);
        }

        public ActionResult RemoveApiClient(Guid id)
        {
            _apiClientService.RemoveApiClient(id);
            return RedirectToAction("ShowAllApiClients");
        }

        [HttpGet]
        public ActionResult EditApiClient(Guid id)
        {
            var apiClientToEdit = _apiClientService.GetApiClientToRemove(id);           
            return View(apiClientToEdit);
        }

        [HttpPost]
        public ActionResult EditApiClient(EditApiClientViewModel editedApiClientVM)
        {
            if (ModelState.IsValid)
            {
                _apiClientService.EditApiClient(editedApiClientVM);
                return RedirectToAction("ShowAllApiClients");
            }
            else
            {
                return View(editedApiClientVM);
            }
        }
    }
}