﻿using System.Web.Mvc;
using WMS.Api.DAL.DatabaseContext;

namespace WMS.Api.Controllers
{
    public class HomeController : Controller
    {
        AppDbContext _db = new AppDbContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Delete()
        {
            _db.Database.Delete();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Create()
        {
            _db.Database.Create();
            return RedirectToAction("Index", "Home");
        }
    }
}