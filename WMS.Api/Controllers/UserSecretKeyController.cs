﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WMS.Api.Services;
using Microsoft.AspNet.Identity;
using WMS.Api.ViewModels.UserSecretKeyViewModels;
using WMS.Api.ViewModels.ApiClientViewModels;
using WMS.Api.DAL.Models.ApiAuthModels;
using WMS.Api.DAL.DatabaseContext;
using System.Linq;

namespace WMS.Api.Controllers
{
    [Authorize]
    public class UserSecretKeyController : Controller
    {
        private ApiClientService _apiClientService;
        private UserSecretKeyService _userSecretKeyService;

        public UserSecretKeyController()
        {
            _apiClientService = new ApiClientService();
            _userSecretKeyService = new UserSecretKeyService();
        }

        public ActionResult Index()
        {
            var apiClients = _apiClientService.GetAllApiClientsNames().ToList();
            return View(apiClients);
        }

        public ActionResult ShowUserSecretKey(Guid apiClientId)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            var userSecretKey = _userSecretKeyService.GetUserSecretKey(userId, apiClientId);            
            return View(userSecretKey);
        }
        
        public ActionResult CreateUserSecretKey(ShowUserSecretKeyViewModel showUserSecretKeyVM)
        {
            if (ModelState.IsValid)
            {
                Guid userId = Guid.Parse(User.Identity.GetUserId());
                var userSecretKey = _userSecretKeyService.GenerateUserSecretKey(userId, showUserSecretKeyVM.ApplicationId);
                showUserSecretKeyVM.SecretKey = userSecretKey;
                return View("ShowUserSecretKey", showUserSecretKeyVM);
            }
            return RedirectToAction("index");
        }
    }
}