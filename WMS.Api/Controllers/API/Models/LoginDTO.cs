﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.Controllers.API.Models
{
    public class LoginDTO
    {
        public string Email { get; set; }
        public string UserSecretKey { get; set; }
    }
}