﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.Controllers.API.Models
{
    public class GetRoomResultDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }   
}