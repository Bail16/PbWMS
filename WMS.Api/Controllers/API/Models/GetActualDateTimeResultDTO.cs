﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.Controllers.API.Models
{
    public class GetActualDateTimeResultDTO : ApiResult
    {
        public GetActualDateTimeResultDTO(bool isSuccess) : base(isSuccess)
        {

        }
            
        public GetActualDateTimeResultDTO(DateTime actualDate, bool isSuccess):this(isSuccess)
        {
            ActualDate = actualDate;
        }
        public DateTime ActualDate { get; set; }
    }
}