﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.Controllers.API.Models
{
    public class LoginResultDTO : ApiResult
    {
        public LoginResultDTO(bool isSuccess) : base(isSuccess)
        {
            UserId = Guid.Empty;
        }

        public LoginResultDTO(bool isSuccess, Guid userId, string name, string surname) : base(isSuccess)
        {
            UserId = userId;
            Name = name;
            Surname = surname;
        }

        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}