﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.Controllers.API.Models
{
    public class GetRoomsResultDto : ApiResult
    {
        public GetRoomsResultDto(bool isSuccess):base (isSuccess)
        {
            Rooms = new List<GetRoomResultDto>();
        }   

        public GetRoomsResultDto(bool isSuccess, List<GetRoomResultDto> rooms):this(isSuccess)
        {
            Rooms = rooms;
        }

        public List<GetRoomResultDto> Rooms { get; set; }

    }
}