﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WMS.Api.Enums;

namespace WMS.Api.Controllers.API.Models
{
    public class CreateTimeLogDTO
    {
        public Guid UserId { get; set; }
       
        public TimeLogType TimeLogEntryType { get; set; }
    }
}