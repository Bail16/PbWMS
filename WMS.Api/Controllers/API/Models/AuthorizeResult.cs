﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.Controllers.API.Models
{
    public class AuthorizeResult
    {
        public AuthorizeResult(bool isAuthorized)
        {
            IsAuthorized = isAuthorized;
        }

        public AuthorizeResult(bool isAuthorized, string name, string surname) : this(isAuthorized)
        {
            Name = name;
            Surname = surname;
        }

        public bool IsAuthorized { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}