﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.Controllers.API.Models
{
    public class GetWorkTimeResultDto : ApiResult
    {
        public GetWorkTimeResultDto(TimeSpan workTime, bool isSuccess) : base(isSuccess)
        {
            WorkTime = workTime.ToString();
        }
        public string WorkTime { get; set; }
    }
}