﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WMS.Api.Enums;

namespace WMS.Api.Controllers.API.Models
{
    public class GetTimeLogResultDTO
    {                
        public DateTime EntryTime { get; set; }
        public DateTime? LeaveTime { get; set; }
        
        public bool IsApproved { get; set; }
        public TimeLogType TimeLogEntryType { get; set; }
        public string TypeCamelCaseName { get; set; }
    }
}