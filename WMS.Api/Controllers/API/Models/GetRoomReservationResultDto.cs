﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.Controllers.API.Models
{
    public class GetRoomReservationResultDto
    {
        public GetRoomReservationResultDto()
        {
            UserName = string.Empty;
            UserSurname = string.Empty;
        }
        public DateTime Date { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public DateTime ReservationDoneAt { get; set; } 
        public string UserName { get; set; }
        public string UserSurname { get; set; } 
    }
}