﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.Controllers.API.Models
{
    public class GetOverTimeResultDto : ApiResult
    {
        public GetOverTimeResultDto(TimeSpan overTime, bool isSuccess) : base(isSuccess)
        {
            OverTime = overTime.ToString();
        }
        public string OverTime { get; set; }
    }
}