﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.Controllers.API.Models
{
    public class CreateRoomReservationResultDto : ApiResult
    {
        public CreateRoomReservationResultDto(bool isSuccess):base(isSuccess)
        {

        }
    }
}