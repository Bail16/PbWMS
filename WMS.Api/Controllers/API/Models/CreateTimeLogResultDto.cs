﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.Controllers.API.Models
{
    public class CreateTimeLogResultDto : ApiResult
    {
        public CreateTimeLogResultDto(bool isSuccess) : base(isSuccess)
        {
            TimeLogId = null;
        }

        public CreateTimeLogResultDto(Guid timeLogId, bool isSuccess) : base(isSuccess)
        {
            TimeLogId = timeLogId;
        }

        public Guid? TimeLogId { get; set; }
    }
}