﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.Controllers.API.Models
{
    public class GetRoomReservationsResultDto : ApiResult   
    {
        public GetRoomReservationsResultDto(bool isSuccess) : base(isSuccess)
        {
            Reservations = new List<GetRoomReservationResultDto>();
        }

        public GetRoomReservationsResultDto(bool isSuccess, List<GetRoomReservationResultDto> reservations) :base(isSuccess)
        {
            Reservations = reservations;
        }

        public List<GetRoomReservationResultDto> Reservations { get; set; }
    }
}