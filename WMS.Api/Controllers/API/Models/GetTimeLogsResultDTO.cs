﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.Controllers.API.Models
{
    public class GetTimeLogsResultDTO : ApiResult
    {
        public GetTimeLogsResultDTO(bool isSuccess):base(isSuccess)
        {
            TimeLogs = new List<GetTimeLogResultDTO>();
        }

        public GetTimeLogsResultDTO(bool isSuccess, List<GetTimeLogResultDTO> timeLogs):this(isSuccess)
        {
            TimeLogs = timeLogs;
        }

        public List<GetTimeLogResultDTO> TimeLogs { get; set; }
    }
}