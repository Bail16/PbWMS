﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMS.Api.Controllers.API.Models;
using WMS.Api.Services;

namespace WMS.Api.Controllers
{
    public class RoomController : Controller
    {
        private RoomService _roomService;
        public RoomController()
        {
            _roomService = new RoomService();
        }

        [HttpGet]
        public ActionResult GetAllRooms()
        {
            var rooms = _roomService.GetAllRooms();
            return Json(rooms, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAllRoomReservations(Guid roomId, DateTime date)
        {
            var reservations = _roomService.GetRoomReservations(roomId, date);
            return Json(reservations, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreateRoomReservation(CreateRoomReservationDto data)
        {
            var result = _roomService.CreateRoomReservation(data);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}