﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMS.Api.Controllers.API.Models;
using WMS.Api.Services;

namespace WMS.Api.Controllers.API
{
    public class TimeLogController : Controller
    {
        private TimeLogsService _timeLogsService;
        private IRequestAccessManager _requestService   ;

        public TimeLogController()
        {
            _timeLogsService = new TimeLogsService();
        }
        
        [HttpGet]
        public ActionResult GetTimeLog(Guid id)
        {
            //if (_requestService.CanRead(Request.Headers))
            //{
                var result = _timeLogsService.GetTimeLog(id);
                return Json(result, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    return Json(false, JsonRequestBehavior.AllowGet);
            //}            
        }

        [HttpGet]
        public ActionResult GetTimeLogs(Guid userId, DateTime startDate, DateTime endDate)
        {
            var result = _timeLogsService.GetTimeLogs(userId, startDate, endDate);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreateTimeLog(CreateTimeLogDTO createTimeLogDTO)
        {
            var createTimeLogResult =_timeLogsService.CreateTimeLog(createTimeLogDTO);
            return Json(createTimeLogResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        public ActionResult UpdateTimeLog(Guid id, UpdateTimeLogDTO updateTimeLogDTO)
        {
            _timeLogsService.UpdateTimeLog(id, updateTimeLogDTO);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpDelete]
        public ActionResult DeleteTimeLog(Guid id)
        {
            _timeLogsService.DeleteTimeLog(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }


    }
}