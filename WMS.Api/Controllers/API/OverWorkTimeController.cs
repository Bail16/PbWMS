﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMS.Api.Services;

namespace WMS.Api.Controllers.API
{
    public class OverWorkTimeController : Controller
    {
        private WorkTimeService _workTimeService;
        private OvertimeService _overtimeService;

        

        public OverWorkTimeController()
        {
            _workTimeService = new WorkTimeService();
            _overtimeService = new OvertimeService();
        }

        [HttpGet]
        public ActionResult GetWorkTime(Guid userId, DateTime startDate, DateTime endDate)
        {
            var result = _workTimeService.GetWorkTime(userId, startDate, endDate);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetOverTime(Guid userId, DateTime startDate, DateTime endDate)
        {
            var result = _overtimeService.GetOverTime(userId, startDate, endDate);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}