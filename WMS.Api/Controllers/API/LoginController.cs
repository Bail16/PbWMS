﻿using System;
using System.Web.Mvc;
using WMS.Api.Controllers.API.Models;
using WMS.Api.Services;

namespace WMS.Api.Controllers
{
    public class LoginController : Controller
    {
        HeadresExtractor _headersExtractor;
        AuthorizeService _authService;

        public LoginController()
        {
            _headersExtractor = new HeadresExtractor();
            _authService = new AuthorizeService();
        }        
        
        [HttpPost]
        public JsonResult Login(LoginDTO data)
        {
            var apiSecretKeyStr = HttpContext.Request.Headers.Get("apiClientSecretKey");
            Guid apiSecretKey = new Guid();
            if(Guid.TryParse(apiSecretKeyStr, out apiSecretKey))
            {
                Guid userId = new Guid();
                var authResult = _authService.Authorize(apiSecretKey, data.Email, data.UserSecretKey, out userId);
                if (authResult.IsAuthorized)
                {
                    return Json(new LoginResultDTO(true, userId, authResult.Name, authResult.Surname), JsonRequestBehavior.AllowGet);
                }
            }           
            return Json(new LoginResultDTO(false), JsonRequestBehavior.AllowGet);
        }


    }
}