﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WMS.Api.Controllers.API.Models;

namespace WMS.Api.Controllers.API
{
    public class TimeController : Controller
    {
        [HttpGet]
        public JsonResult Index()
        {
            var result = new GetActualDateTimeResultDTO(DateTime.Now, true);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}