﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.ViewModels.UserSecretKeyViewModels
{
    public class ShowUserSecretKeyViewModel
    {
        public Guid ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public string SecretKey { get; set; }
    }
}