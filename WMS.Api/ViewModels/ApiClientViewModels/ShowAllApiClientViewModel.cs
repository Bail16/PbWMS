﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WMS.Api.ViewModels.ApiClientViewModels
{
    public class ShowAllApiClientViewModel
    {
        public Guid Id { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Can read")]
        public bool CanRead { get; set; }

        [Display(Name = "Can write")]
        public bool CanWrite { get; set; }
    }
}