﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.ViewModels.ApiClientViewModels
{
    public class ShowAllApiClientNameViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}