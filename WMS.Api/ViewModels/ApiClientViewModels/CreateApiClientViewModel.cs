﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WMS.Api.ViewModels.ApiClientViewModels
{
    public class CreateApiClientViewModel
    {
        public CreateApiClientViewModel()
        {
            CanRead = true;
            CanWrite = true;
        }
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Display(Name = "Can read")]
        public bool CanRead { get; set; }

        [Display(Name = "Can write")]
        public bool CanWrite { get; set; }
    }
}