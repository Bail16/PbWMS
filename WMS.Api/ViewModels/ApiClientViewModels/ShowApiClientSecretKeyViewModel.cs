﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.ViewModels.ApiClientViewModels
{
    public class ShowApiClientSecretKeyViewModel
    {
        public string Name { get; set; }
        public Guid SecretKey { get; set; }
    }
}