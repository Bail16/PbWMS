﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.Helpers
{
    public static class UserSecretKeyGenerator
    {
        private static Random random = new Random();

        public static string GenerateSecretKey(int length = 6)
        {
            const string chars = "abcdefghijklmnoprstuwyxzv0123456789";
            var secretString = new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
            return secretString;
        }
    }
}