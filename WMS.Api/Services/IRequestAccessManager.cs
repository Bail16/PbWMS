﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Api.Services
{
    public interface IRequestAccessManager
    {
        bool CanRead(NameValueCollection headers);
        bool CanWrite(NameValueCollection headers);
    }
}
