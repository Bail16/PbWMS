﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WMS.Api.Controllers.API.Models;
using WMS.Api.DAL.Models;
using WMS.Api.DAL.Models.TimeLogModels;
using WMS.Api.DAL.Providers;
using WMS.Api.DAL.Providers.Abstract;
using WMS.Api.Enums;

namespace WMS.Api.Services
{
    public class TimeLogsService
    {
        ITimeLogTypeProvider _timeLogTypeProvider;
        ITimeLogEntryProvider _timeLogProvider;
        IAppLogProvider _appLogProvider;

        public TimeLogsService()
        {
            _timeLogTypeProvider = new TimeLogTypeProvider();
            _timeLogProvider = new TimeLogEntryProvider();
            _appLogProvider = new AppLogProvider();
        }

        public CreateTimeLogResultDto CreateTimeLog(CreateTimeLogDTO createTimeLogDTO)
        {
            var now = DateTime.Now;
            if(now.TimeOfDay.Seconds > 0)
            {
                now.Add(TimeSpan.FromMinutes(-1));
            }              
            
            var timeLogToAdd = new TimeLogEntry
            {
                EntryTime = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0, 0),
                TimeLogEntryTypeId = _timeLogTypeProvider.GetTimeLogTypeId(createTimeLogDTO.TimeLogEntryType),
                UserId = createTimeLogDTO.UserId
            };          
            _timeLogProvider.Create(timeLogToAdd);
            return new CreateTimeLogResultDto(timeLogToAdd.Id, true);
        }

        public GetTimeLogResultDTO GetTimeLog(Guid id)
        {
            var timeLogEntity = _timeLogProvider.Get(id);
            if(timeLogEntity != null)
            {
                var result = new GetTimeLogResultDTO
                {
                    EntryTime = timeLogEntity.EntryTime,
                    LeaveTime = timeLogEntity.LeaveTime,
                    IsApproved = timeLogEntity.IsApproved,
                    TimeLogEntryType = (TimeLogType)Enum.Parse(typeof(TimeLogType), timeLogEntity.TimeLogEntryType.CamelCaseName),
                    TypeCamelCaseName = timeLogEntity.TimeLogEntryType.CamelCaseName
                };
                return result;
            }
            return null;           
        }

        public GetTimeLogsResultDTO GetTimeLogs(Guid userId, DateTime startDate, DateTime endDate)
        {
            var timeLogEntities = _timeLogProvider.Get(userId, startDate, endDate);
            var timeLogs = new List<GetTimeLogResultDTO>();
            foreach (var entity in timeLogEntities)
            {
                timeLogs.Add(new GetTimeLogResultDTO
                {
                    EntryTime = entity.EntryTime,
                    LeaveTime = entity.LeaveTime,
                    IsApproved = entity.IsApproved,
                    TimeLogEntryType = (TimeLogType)Enum.Parse(typeof(TimeLogType), entity.TimeLogEntryType.CamelCaseName),
                    TypeCamelCaseName = entity.TimeLogEntryType.CamelCaseName
                });
            }
            return new GetTimeLogsResultDTO(true, timeLogs);
        }

        public void UpdateTimeLog(Guid id, UpdateTimeLogDTO updateTimeLogDTO)
        {
            var now = DateTime.Now;
            if (now.TimeOfDay.Seconds > 0)
            {
                now.Add(TimeSpan.FromMinutes(1));
            }

            var timeLogToUpdate = _timeLogProvider.Get(id);
            if(timeLogToUpdate != null && timeLogToUpdate.LeaveTime == null)
            {
                timeLogToUpdate.LeaveTime = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0, 0);
                _timeLogProvider.Update(timeLogToUpdate);
            }
        }

        public void DeleteTimeLog(Guid id)
        {
            _timeLogProvider.Remove(id);            
        }

    }
}