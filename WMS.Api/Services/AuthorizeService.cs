﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WMS.Api.Controllers.API.Models;
using WMS.Api.DAL.Providers;

namespace WMS.Api.Services
{
    public class AuthorizeService
    {
        AuthorizeProvider _authProvider;

        public AuthorizeService()
        {
            _authProvider = new AuthorizeProvider();
        }

        public AuthorizeResult Authorize(Guid apiClientSecretKey, string email, string userSecretKey, out Guid userId)
        {
            userId = Guid.Empty;
            var userSecretKeyEnt = _authProvider.GetUserKeyForAuthorization(userSecretKey);
            if (userSecretKeyEnt != null && userSecretKeyEnt.RestApiClient.SecretKey == apiClientSecretKey && userSecretKeyEnt.User.Email == email)
            {
                userId = userSecretKeyEnt.UserId;
                return new AuthorizeResult(true, userSecretKeyEnt.User.Name, userSecretKeyEnt.User.Surname);
            }
            else return new AuthorizeResult(false);
        }                
    }
}