﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WMS.Api.Controllers.API.Models;
using WMS.Api.DAL.Providers;
using WMS.Api.DAL.Providers.Abstract;

namespace WMS.Api.Services
{
    public class WorkTimeService
    {
        ITimeLogEntryProvider _timeLogEntryProvider;

        public WorkTimeService()
        {
            _timeLogEntryProvider = new TimeLogEntryProvider();
        }
        public GetWorkTimeResultDto GetWorkTime(Guid userId, DateTime startDate, DateTime endDate)
        {
            var workTime = TimeSpan.Zero;
            var userTimeLogs = _timeLogEntryProvider.Get(userId, startDate, endDate);
            foreach (var timeLog in userTimeLogs)
            {                
                var now = DateTime.Now;
                var logLeaveTime = timeLog.LeaveTime ?? new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0, 0);
                var workTimeToAdd = logLeaveTime - timeLog.EntryTime;
                workTime = workTime.Add(workTimeToAdd);
            }
            return new GetWorkTimeResultDto(workTime, true);
        }
    }
}