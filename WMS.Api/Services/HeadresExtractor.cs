﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WMS.Api.Services
{
    public class HeadresExtractor
    {
        private string _clientHeaderSecretKeyName => "ApiClientSecretKey";
        public Guid GetApiClientSecretKey(HttpRequestBase request)
        {
            var secretKeyInHeader = request.Headers.Get(_clientHeaderSecretKeyName);
            var secretKeyGuid = Guid.Empty;
            Guid.TryParse(secretKeyInHeader, out secretKeyGuid);
            return secretKeyGuid;
        }
    }
}