﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WMS.Api.Controllers.API.Models;
using WMS.Api.DAL.Providers;
using WMS.Api.DAL.Providers.Abstract;

namespace WMS.Api.Services
{
    public class OvertimeService
    {
        IPublicHolidaysProvider _publicHolidaysProvider;
        WorkTimeService _workTimeService;
        public OvertimeService()
        {
            _publicHolidaysProvider = new PublicHolidaysProvider();
            _workTimeService = new WorkTimeService();
        }
        
        public GetOverTimeResultDto GetOverTime(Guid userId, DateTime startDate, DateTime endDate)
        {
            var workTimeResult = _workTimeService.GetWorkTime(userId, startDate, endDate);
            var workTime = TimeSpan.Parse(workTimeResult.WorkTime);
            var expectedWorkTime = GetExpectedWorkTime(startDate, endDate);
            return new GetOverTimeResultDto((workTime - expectedWorkTime), true);
        }
                

        private TimeSpan GetExpectedWorkTime(DateTime startDate, DateTime endDate)
        {
            var expected = TimeSpan.Zero;
            var publicHolidays = _publicHolidaysProvider.GetPublicHolidays(startDate, endDate);
            foreach (DateTime day in EachWorkingDay(startDate, endDate))
            {
                if (!publicHolidays.Any(n => n.Date == day.Date))
                {
                    expected = expected.Add(TimeSpan.FromHours(8));
                }
            }
            return expected;
        }

        private IEnumerable<DateTime> EachWorkingDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
            {
                if(day.DayOfWeek == DayOfWeek.Saturday || day.DayOfWeek == DayOfWeek.Sunday)                
                    continue;                
                else
                    yield return day;
            }
        }
    }
}