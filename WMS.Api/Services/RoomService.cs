﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WMS.Api.Controllers.API.Models;
using WMS.Api.DAL.Models;
using WMS.Api.DAL.Providers;

namespace WMS.Api.Services
{
    public class RoomService
    {
        private RoomProvider _roomProvider;
        public RoomService()
        {
            _roomProvider = new RoomProvider();
        }

        public GetRoomsResultDto GetAllRooms()
        {
            var rooms = _roomProvider.GetAllRooms();
            var roomsResult = new List<GetRoomResultDto>();
            foreach (var room in rooms)
            {
                roomsResult.Add(new GetRoomResultDto
                {
                    Id = room.Id,
                    Name = room.RoomName
                });
            }
            var result = new GetRoomsResultDto(true, roomsResult);
            return result;
        }

        public GetRoomReservationsResultDto GetRoomReservations(Guid roomId, DateTime date)
        {
            var roomReservationsResult = new List<GetRoomReservationResultDto>();
            var roomReservations = _roomProvider.GetAllRoomReservations(date, roomId);
            foreach (var reservation in roomReservations)
            {
                roomReservationsResult.Add(new GetRoomReservationResultDto
                {
                    TimeFrom = reservation.TimeFrom.ToString(),
                    TimeTo = reservation.TimeTo.ToString(),
                    Date = reservation.Date,
                    ReservationDoneAt = reservation.Timestamp.Value,
                    UserName = reservation.User?.Name,
                    UserSurname = reservation.User?.Surname
                });
            }
            var result = new GetRoomReservationsResultDto(true, roomReservationsResult);
            return result;
        }


        public CreateRoomReservationResultDto CreateRoomReservation(CreateRoomReservationDto data)
        {
            var roomReservation = new RoomReservation
            {
                Date = data.Date,
                TimeFrom = data.TimeFrom,
                TimeTo = data.TimeTo
            };
            _roomProvider.AddRoomReservation(roomReservation, data.RoomId);

            return new CreateRoomReservationResultDto(true);
        }
        
    }
}