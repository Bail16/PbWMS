﻿using System;
using System.Linq;
using WMS.Api.DAL.DatabaseContext;
using WMS.Api.DAL.Models.ApiAuthModels;
using WMS.Api.DAL.Providers;
using WMS.Api.Helpers;
using WMS.Api.ViewModels.UserSecretKeyViewModels;

namespace WMS.Api.Services
{
    public class UserSecretKeyService
    {
        private UserSecretKeyProvider _userSecretKeyProvider;
        private RestApiClientProvider _restApiClientProvider;

        public UserSecretKeyService()
        {
            _userSecretKeyProvider = new UserSecretKeyProvider();
            _restApiClientProvider = new RestApiClientProvider();
        }

        public ShowUserSecretKeyViewModel GetUserSecretKey(Guid userId, Guid apiClientId)
        {
            var userSecretKey = _userSecretKeyProvider.Get(userId, apiClientId);
            var apiClient = _restApiClientProvider.Get(apiClientId);
            var result = new ShowUserSecretKeyViewModel
            {
                ApplicationName = apiClient.Name,
                ApplicationId = apiClient.Id
            };

            if (userSecretKey != null)
            {
                result.SecretKey = userSecretKey.SecretKey;
            }
            else
            {
                result.SecretKey = string.Empty;
            }
            return result;
        }
        
        public string GenerateUserSecretKey(Guid userId, Guid apiClientId)
        {
            var existingSecretKey = _userSecretKeyProvider.Get(userId, apiClientId);
            var newSecretKey = UserSecretKeyGenerator.GenerateSecretKey();

            if (existingSecretKey != null)
            {
                existingSecretKey.SecretKey = newSecretKey;
                _userSecretKeyProvider.Edit(existingSecretKey);
            }
            else
            {
                UserKey userSecretKey = new UserKey
                {
                    SecretKey = newSecretKey
                };
                _userSecretKeyProvider.Add(userId, apiClientId, userSecretKey);
            }
            return newSecretKey;
                        

            //CODE WICH DON'T REMOVE OLD USER KEYS
            //var existingSecretKey = _context.UserKeys.FirstOrDefault(n => n.UserId == userId && n.RestApiClientId == apiClientId && n.IsDeleted == false);
            //var user = _context.Users.FirstOrDefault(n => n.Id == userId);
            //var apiClient = _context.RestApiClients.FirstOrDefault(n => n.Id == apiClientId && n.IsDeleted == false);
            //UserKey userSecretKey = new UserKey
            //{
            //    User = user,
            //    RestApiClient = apiClient,
            //    SecretKey = UserSecretKeyGenerator.GenerateSecretKey()
            //};
            //if (existingSecretKey != null)
            //{
            //    existingSecretKey.IsDeleted = true;                
            //}
            //_context.UserKeys.Add(userSecretKey);
            //_context.SaveChanges();
            //return userSecretKey.SecretKey;
        }
    }
}