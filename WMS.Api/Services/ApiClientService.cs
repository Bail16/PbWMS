﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using WMS.Api.DAL.DatabaseContext;
using WMS.Api.DAL.Models.ApiAuthModels;
using WMS.Api.DAL.Providers;
using WMS.Api.ViewModels.ApiClientViewModels;
using WMS.Api.ViewModels.UserSecretKeyViewModels;

namespace WMS.Api.Services
{
    public class ApiClientService
    {
        RestApiClientProvider _restApiClientProvider;
        UserSecretKeyProvider _userSecretKeyProider;

        public ApiClientService()
        {
            _userSecretKeyProider = new UserSecretKeyProvider();
            _restApiClientProvider = new RestApiClientProvider();
        }

        public Guid CreateApiClient(CreateApiClientViewModel createApiClientViewModel)
        {
            RestApiClient apiClient = new RestApiClient
            {
                Name = createApiClientViewModel.Name,
                CanRead = createApiClientViewModel.CanRead,
                CanWrite = createApiClientViewModel.CanWrite
            };
            _restApiClientProvider.Add(apiClient);
            return apiClient.Id;
        }

        public ShowApiClientSecretKeyViewModel GetSecretKey(Guid apiClientId)
        {
            var apiClient = _restApiClientProvider.Get(apiClientId);
            if (apiClient != null)
            {
                var result = new ShowApiClientSecretKeyViewModel
                {
                    SecretKey = apiClient.SecretKey,
                    Name = apiClient.Name
                };
                return result;
            }
            return null;
        }

        public EditApiClientViewModel GetApiClientToRemove(Guid id)
        {
            var apiClientToEdit = _restApiClientProvider.Get(id);
            if (apiClientToEdit != null)
            {
                var result = new EditApiClientViewModel
                {
                    Id = apiClientToEdit.Id,
                    Name = apiClientToEdit.Name,
                    CanRead = apiClientToEdit.CanRead,
                    CanWrite = apiClientToEdit.CanWrite
                };
                return result;
            }
            else return null;
        }

        public IEnumerable<ShowAllApiClientNameViewModel> GetAllApiClientsNames()
        {
            var allApiClients = _restApiClientProvider.GetAll().ToList();
            var result = new List<ShowAllApiClientNameViewModel>();
            allApiClients.ForEach(n => result.Add(new ShowAllApiClientNameViewModel
            {
                Id = n.Id,
                Name = n.Name
            }));
            return result;
        }

        public IEnumerable<ShowAllApiClientViewModel> GetAllApiClients()
        {
            var allApiClients = _restApiClientProvider.GetAll().ToList();
            var result = new List<ShowAllApiClientViewModel>();
            allApiClients.ForEach(n => result.Add(new ShowAllApiClientViewModel
            {
                Id = n.Id,
                Name = n.Name,
                CanRead = n.CanRead,
                CanWrite = n.CanWrite
            }));
            return result;
        }


        //public ShowUserSecretKeyViewModel GetApiClient(Guid id)
        //{
        //    var apiClient = _restApiClientProvider.Get(id);
        //    var userSecretKey = _userSecretKeyProider.Get(userId, apiClientId);
        //    var result = new ShowUserSecretKeyViewModel
        //    {
        //        SecretKey = userSecretKey,
        //        ApplicationId = apiClient.Id,
        //        ApplicationName = apiClient.Name
        //    };

        //    return _context.RestApiClients.FirstOrDefault(n => n.Id == id && n.IsDeleted == false);
        //}

        public void EditApiClient(EditApiClientViewModel editedApiClient)
        {

            var editedApiClientEntity = new RestApiClient
            {
                CanRead = editedApiClient.CanRead,
                CanWrite = editedApiClient.CanWrite,
                Id = editedApiClient.Id,
                Name = editedApiClient.Name
            };
            _restApiClientProvider.Edit(editedApiClientEntity);
        }

        public void RemoveApiClient(Guid apiClientId)
        {
            _restApiClientProvider.Remove(apiClientId);
        }
    }
}