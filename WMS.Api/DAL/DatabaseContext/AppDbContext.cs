﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using WMS.Api.DAL.Models;
using WMS.Api.DAL.Models.TimeLogModels;
using WMS.Api.DAL.Models.ApiAuthModels;

namespace WMS.Api.DAL.DatabaseContext
{
    public class AppDbContext : IdentityDbContext<ApplicationUser, GuidRole, Guid, GuidUserLogin, GuidUserRole, GuidUserClaim>
    {
        public AppDbContext():base("ConnString")
        {
            Database.SetInitializer(new DatabaseInitializer());   
        }

        public DbSet<AppLog> AppLogs { get; set; }

        public DbSet<TimeLogEntry> TimeLogEntries { get; set; }

        public DbSet<TimeLogEntryType> TimeLogEntryTypes { get; set; }

        public DbSet<PublicHoliday> PublicHolidays { get; set; }

        public DbSet<RestApiClient> RestApiClients { get; set; }

        public DbSet<UserKey> UserKeys { get; set; }

        public DbSet<Room> Rooms { get; set; }
        public DbSet<RoomReservation> RoomReservations { get; set; }

        public static AppDbContext Create()
        {
            return new AppDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
} 