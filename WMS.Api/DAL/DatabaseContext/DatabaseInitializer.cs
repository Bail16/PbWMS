﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using WMS.Api.DAL.Models;
using WMS.Api.DAL.Models.ApiAuthModels;
using WMS.Api.DAL.Models.TimeLogModels;

namespace WMS.Api.DAL.DatabaseContext
{
    public class DatabaseInitializer : DropCreateDatabaseIfModelChanges<AppDbContext>
    {
        protected override void Seed(AppDbContext context)
        {
            List<TimeLogEntryType> timeLogEntryTypes = new List<TimeLogEntryType>
            {
                new TimeLogEntryType
                {
                    Name = "Forgot to log in",
                    ReduceTime = 0,
                    NeedsConfirmation = true,
                    IsSelectable = true,
                    IsDeleted = false,
                    CamelCaseName = "ForgotToLogIn"
                },
                new TimeLogEntryType
                {
                    Name = "Day off",
                    ReduceTime = 480,
                    NeedsConfirmation = true,
                    IsSelectable = true,
                    IsDeleted = false,
                    CamelCaseName = "DayOff"
                },
                new TimeLogEntryType
                {
                    Name = "Delegation",
                    ReduceTime = 480,
                    NeedsConfirmation = true,
                    IsSelectable = true,
                    IsDeleted = false,
                    CamelCaseName = "Delegation"
                },
                new TimeLogEntryType
                {
                    Name = "Half day off",
                    ReduceTime = 240,
                    NeedsConfirmation = true,
                    IsSelectable = true,
                    IsDeleted = false,
                    CamelCaseName = "HalfDayOff"
                },
                new TimeLogEntryType
                {
                    Name = "Regular",
                    ReduceTime = 0,
                    NeedsConfirmation = false,
                    IsSelectable = false,
                    IsDeleted = false,
                    CamelCaseName = "Regular"
                },
                new TimeLogEntryType
                {
                    Name = "Sick leave",
                    ReduceTime = 480,
                    NeedsConfirmation = true,
                    IsSelectable = true,
                    IsDeleted = false,
                    CamelCaseName = "SickLeave"
                },
                new TimeLogEntryType
                {
                    Name = "Forgot to log out",
                    ReduceTime = 0,
                    NeedsConfirmation = true,
                    IsSelectable = false,
                    IsDeleted = false,
                    CamelCaseName = "ForgotToLogOut"
                }
            };
            context.TimeLogEntryTypes.AddRange(timeLogEntryTypes);

            List<Room> rooms = new List<Room>
            {
                new Room
                {
                    RoomName = "Room 1"
                },
                 new Room
                {
                    RoomName = "Room 2"
                },
                  new Room
                {
                    RoomName = "Room 3"
                },
                new Room
                {
                    RoomName = "Conference room"
                }
            };
            context.Rooms.AddRange(rooms);

            base.Seed(context);
            context.SaveChanges();
        }
    }
}