﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WMS.Api.DAL.DatabaseContext;
using WMS.Api.DAL.Models.TimeLogModels;
using WMS.Api.DAL.Providers.Abstract;

namespace WMS.Api.DAL.Providers
{
    public class PublicHolidaysProvider : IPublicHolidaysProvider
    {
        public IEnumerable<PublicHoliday> GetPublicHolidays(DateTime startDate, DateTime endDate)
        {
            using(var context = new AppDbContext())
            {
                var holidays = context.PublicHolidays.Where(n => n.IsDeleted == false && n.Date >= startDate && n.Date <= endDate);
                return holidays.ToList();
            }
        }
    }
}