﻿using System;
using System.Collections.Generic;
using WMS.Api.DAL.Models.TimeLogModels;

namespace WMS.Api.DAL.Providers.Abstract
{
    public interface IPublicHolidaysProvider
    {
        IEnumerable<PublicHoliday> GetPublicHolidays(DateTime startDate, DateTime endDate);
    }
}