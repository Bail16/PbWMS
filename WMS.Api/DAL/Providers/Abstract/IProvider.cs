﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Api.DAL.Providers.Abstract
{
     public interface IProvider<T>
    {
        T Get(Guid id);
        IEnumerable<T> GetAll();
        void Add(T entity);
        void Remove(Guid id);
        void Edit(Guid id, T entity);
    }
}
