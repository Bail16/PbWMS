﻿using System;
using System.Collections.Generic;
using WMS.Api.DAL.Models.TimeLogModels;

namespace WMS.Api.DAL.Providers.Abstract
{
    public interface ITimeLogEntryProvider
    {
        void Create(TimeLogEntry timeLogEntry);
        TimeLogEntry Get(Guid id);
        IEnumerable<TimeLogEntry> Get(Guid userId, DateTime startDate, DateTime endDate);
        //IEnumerable<TimeLogEntry> GetForWorkTime(Guid userId, DateTime startDate, DateTime endDate);
        void Update(TimeLogEntry timeLogEntry);
        void Remove(Guid id);
    }
}