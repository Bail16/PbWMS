﻿using System;
using WMS.Api.DAL.Models.TimeLogModels;
using WMS.Api.Enums;

namespace WMS.Api.DAL.Providers.Abstract
{
    public interface ITimeLogTypeProvider
    {
        Guid GetTimeLogTypeId(TimeLogType type);

        TimeLogEntryType Get(Guid id);
    }
}