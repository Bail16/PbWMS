﻿using WMS.Api.DAL.Models;

namespace WMS.Api.DAL.Providers.Abstract
{
    public interface IAppLogProvider
    {
        void Create(AppLog appLog);
    }
}