﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using WMS.Api.DAL.DatabaseContext;
using WMS.Api.DAL.Models;

namespace WMS.Api.DAL.Providers
{
    public class RoomProvider
    {
        public IEnumerable<RoomReservation> GetAllRoomReservations(DateTime date, Guid roomId)
        {
            //using (var context = new AppDbContext())
            //{
            //    var roomReservations = context.RoomReservations
            //        .Include(b => b.User)
            //        .Where(n => DbFunctions.TruncateTime(n.Date) == date.Date && n.RoomId == roomId && !n.IsDeleted);
            //    return roomReservations.ToList();
            //}

            return new List<RoomReservation>
                    {
                        new RoomReservation
                        {
                          Date = date,
                          TimeFrom = DateTime.Now.TimeOfDay,
                          TimeTo = DateTime.Now.TimeOfDay.Add(TimeSpan.FromHours(1)),
                           User = new Models.ApiAuthModels.ApplicationUser
                          {
                              Name = "Patrycja",
                              Surname = "Furtak"
                          }
                        },
                        new RoomReservation
                        {
                          Date = date,
                          TimeFrom = DateTime.Now.TimeOfDay.Add(TimeSpan.FromHours(2)),
                          TimeTo = DateTime.Now.TimeOfDay.Add(TimeSpan.FromHours(4)),
                          User = new Models.ApiAuthModels.ApplicationUser
                          {
                              Name = "Kamil",
                              Surname = "Zemczak"
                          }
                        }
                    };
        }

        public void AddRoomReservation(RoomReservation roomReservation, Guid roomId)
        {
            using (var context = new AppDbContext())
            {
                var room = context.Rooms.Find(roomId);
                if (room != null && !room.IsDeleted)
                {
                    room.Reservations.Add(roomReservation);
                }
            }
        }

        public IEnumerable<Room> GetAllRooms()
        {
            using (var context = new AppDbContext())
            {
                var rooms = context.Rooms.Where(n => !n.IsDeleted);
                return rooms.ToList();
            }
        }
    }
}