﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WMS.Api.DAL.DatabaseContext;
using WMS.Api.DAL.Models.ApiAuthModels;
using WMS.Api.DAL.Providers.Abstract;

namespace WMS.Api.DAL.Providers
{
    public class UserSecretKeyProvider
    {
        public void Add(Guid userId, Guid apiClientId, UserKey entity)
        {
            using (var context = new AppDbContext())
            {
                var user = context.Users.Find(userId);
                var apiClient = context.RestApiClients.Find(apiClientId);

                entity.User = user;
                entity.RestApiClient = apiClient;

                context.UserKeys.Add(entity);
                context.SaveChanges();
            }
        }

        public void Edit(UserKey entity)
        {
            using (var context = new AppDbContext())
            {
                if (entity != null)
                {
                    context.UserKeys.Attach(entity);
                    context.Entry(entity).State = EntityState.Modified;
                    context.SaveChanges();
                }
            }
        }

        public UserKey Get(Guid userId, Guid apiClientId)
        {
            using (var context = new AppDbContext())
            {
                var userKey = context.UserKeys.FirstOrDefault(n => n.UserId == userId && n.RestApiClientId == apiClientId && n.RestApiClient.IsDeleted == false);
                if (userKey != null)
                {
                    return userKey;
                }
            }
            return null;
        }
        
        public void Remove(Guid id)
        {
            using (var context = new AppDbContext())
            {
                var userKey = context.UserKeys.Find(id);
                if(userKey != null)
                {
                    context.UserKeys.Attach(userKey);
                    userKey.IsDeleted = true;
                    context.SaveChanges();
                }
            }
        }
    }
}