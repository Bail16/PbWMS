﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WMS.Api.DAL.DatabaseContext;
using WMS.Api.DAL.Models;
using WMS.Api.DAL.Models.TimeLogModels;
using WMS.Api.DAL.Providers.Abstract;

namespace WMS.Api.DAL.Providers
{
    public class TimeLogEntryProvider : ITimeLogEntryProvider
    {
        public TimeLogEntry Get(Guid id)
        {
            using(var context = new AppDbContext())
            {
                var timeLog = context.TimeLogEntries
                    .Include(n=>n.TimeLogEntryType)
                    .FirstOrDefault(
                        n => n.Id == id 
                        &&
                        n.IsDeleted == false);

                return timeLog;
            }
        }

        public IEnumerable<TimeLogEntry> Get(Guid userId, DateTime startDate, DateTime endDate)
        {
            endDate = endDate.AddDays(1);
            var now = DateTime.Now;
            using(var context = new AppDbContext())
            {
                var timeLogs = context.TimeLogEntries
                     .Include(n => n.TimeLogEntryType)
                     .Where(
                         n => n.UserId == userId
                         &&
                         n.EntryTime >= startDate
                         &&
                         (   //Statement if leavetime is null assert leavetime to now
                         (n.LeaveTime.HasValue && n.LeaveTime.Value < endDate)
                                 ||
                         (!n.LeaveTime.HasValue && now < endDate)
                         )
                         &&
                         n.IsDeleted == false);


                return timeLogs.ToList();
            }
        }

        public void Create(TimeLogEntry timeLogEntry)
        {
            using(var context = new AppDbContext())
            {
                var user = context.Users.Find(timeLogEntry.UserId);
                timeLogEntry.User = user;

                var timeLogType = context.TimeLogEntryTypes.Find(timeLogEntry.TimeLogEntryTypeId);
                timeLogEntry.TimeLogEntryType = timeLogType;
                timeLogEntry.IsApproved = !timeLogType.NeedsConfirmation;

                timeLogEntry.AppLog = GetAppLog(timeLogEntry, 0);          
                
                context.TimeLogEntries.Add(timeLogEntry);
                context.SaveChanges();
            }
        }


        public void Update(TimeLogEntry timeLogEntry)
        {
            using(var context = new AppDbContext())
            {
                var toEdit = context.TimeLogEntries.FirstOrDefault(n => n.Id == timeLogEntry.Id);
                if(toEdit != null)
                {
                    context.Entry(toEdit).CurrentValues.SetValues(timeLogEntry);
                    context.SaveChanges();
                }
            }
        }

        public void Remove(Guid id)
        {
            using(var context = new AppDbContext())
            {
                var timeLog = context.TimeLogEntries.FirstOrDefault(n => n.Id == id);
                if(timeLog != null)
                {
                    context.TimeLogEntries.Attach(timeLog);
                    timeLog.IsDeleted = true;
                    context.SaveChanges();
                }
            }
        }        

        private AppLog GetAppLog(TimeLogEntry timeLogEntry, int actionType)
        {
            return new AppLog
            {
                ObjectId = timeLogEntry.Id,
                ObjectJson = JsonConvert.SerializeObject(timeLogEntry),
                User = timeLogEntry.User,
                ObjectType = timeLogEntry.GetType().ToString(),
                ActionType = actionType
            };
        }
    }
}