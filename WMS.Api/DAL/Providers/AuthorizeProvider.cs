﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WMS.Api.DAL.DatabaseContext;
using WMS.Api.DAL.Models.ApiAuthModels;

namespace WMS.Api.DAL.Providers
{
    public class AuthorizeProvider
    {
        public UserKey GetUserKeyForAuthorization(string userSecretKey)
        {
            using(var context = new AppDbContext())
            {
                var result = context.UserKeys
                    .Include(u =>u.User)
                    .Include(c=>c.RestApiClient)
                    .FirstOrDefault(n => n.SecretKey == userSecretKey);
                return result;
            }
        }
    }
}