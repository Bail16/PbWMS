﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WMS.Api.DAL.DatabaseContext;
using WMS.Api.DAL.Models;
using WMS.Api.DAL.Providers.Abstract;

namespace WMS.Api.DAL.Providers
{
    public class AppLogProvider : IAppLogProvider
    {
        public void Create(AppLog appLog)
        {
            using(var context = new AppDbContext())
            {
                var user = context.Users.FirstOrDefault(n => n.Id == appLog.UserId);
                if(user != null)
                {
                    appLog.User = user;
                    context.AppLogs.Add(appLog);
                    context.SaveChanges();
                }
            }
        }
    }
}