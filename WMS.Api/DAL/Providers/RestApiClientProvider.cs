﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WMS.Api.DAL.DatabaseContext;
using WMS.Api.DAL.Models.ApiAuthModels;

namespace WMS.Api.DAL.Providers
{
    public class RestApiClientProvider
    {
        public void Add(RestApiClient entity)
        {
            using (var context = new AppDbContext())
            {
                context.RestApiClients.Add(entity);
                context.SaveChanges();
            }
        }

        public RestApiClient Get(Guid id)
        {
            using (var context = new AppDbContext())
            {
                var restApiClient = context.RestApiClients.Find(id);
                if (restApiClient != null)
                    return restApiClient;
            }
            return null;
        }

        public IEnumerable<RestApiClient> GetAll()
        {
            using (var context = new AppDbContext())
            {
                return context.RestApiClients.Where(n=>n.IsDeleted == false).ToList();
            }
        }

        public void Remove(Guid id)
        {
            using (var context = new AppDbContext())
            {
                var apiClient = context.RestApiClients.Find(id);
                var userSecretKeys = context.UserKeys.Where(n => n.RestApiClientId == id);
                if (userSecretKeys != null)
                {
                    context.UserKeys.RemoveRange(userSecretKeys);
                    context.SaveChanges();
                }
                context.RestApiClients.Attach(apiClient);
                apiClient.IsDeleted = true;
                context.SaveChanges();
            }
        }

        public void Edit(RestApiClient restApiClient)
        {
            using(var context = new AppDbContext())
            {
                var oldRestApiClient = context.RestApiClients.Find(restApiClient.Id);
                context.Entry(oldRestApiClient).CurrentValues.SetValues(restApiClient);
                context.SaveChanges();
            }
        }        
    }
}