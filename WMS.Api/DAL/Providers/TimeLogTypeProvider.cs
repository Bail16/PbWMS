﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WMS.Api.DAL.DatabaseContext;
using WMS.Api.DAL.Models.TimeLogModels;
using WMS.Api.DAL.Providers.Abstract;
using WMS.Api.Enums;

namespace WMS.Api.DAL.Providers
{
    public class TimeLogTypeProvider : ITimeLogTypeProvider
    {
        public TimeLogEntryType Get(Guid id)
        {
            using(var context = new AppDbContext())
            {
                var timeLogType = context.TimeLogEntryTypes.FirstOrDefault(n => n.Id == id);
                return timeLogType;
            }
        }

    
        public Guid GetTimeLogTypeId(TimeLogType type)
        {
            using(var context = new AppDbContext())
            {
                var enumName = Enum.GetName(typeof(TimeLogType), type);
                var timeLogType = context.TimeLogEntryTypes.FirstOrDefault(n => n.CamelCaseName == enumName);
                if(timeLogType == null)
                {
                    throw new Exception("Not recognized type!");
                }
                else
                {
                    return timeLogType.Id;
                }
            }
        }
    }
}