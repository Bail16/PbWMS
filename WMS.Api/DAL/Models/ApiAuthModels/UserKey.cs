﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WMS.Api.DAL.Models.ApiAuthModels
{
    public class UserKey : Entity
    {
        [Required]
        public Guid UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; } 

        [Required]
        public Guid RestApiClientId { get; set; } 

        [ForeignKey("RestApiClientId")]
        public virtual RestApiClient RestApiClient { get; set; }

        [Required]
        [MaxLength(36)]
        public string SecretKey { get; set; }
    }
}