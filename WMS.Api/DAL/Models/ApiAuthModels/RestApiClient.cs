﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WMS.Api.DAL.Models.ApiAuthModels
{
    public class RestApiClient : Entity 
    {
        public RestApiClient()
        {
            SecretKey = Guid.NewGuid();
        }


        [Required]
        [MaxLength(255)]
        public string Name { get; set; } 

        [Required]
        public Guid SecretKey { get; set; } 

        [Required]
        public bool CanRead { get; set; }

        [Required]
        public bool CanWrite { get; set; }
    }
}