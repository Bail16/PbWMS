﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WMS.Api.DAL.Models.ApiAuthModels;

namespace WMS.Api.DAL.Models
{
    public class AppLog: Entity
    {
        public AppLog()
        {
            CreateDate = DateTime.Now;
        }

        [Required]
        public DateTime CreateDate { get; set; }
        
        public Guid UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        [MaxLength(512)]
        public string ObjectType { get; set; }

        [Required]
        public Guid ObjectId { get; set; }

        [Required]
        public int ActionType { get; set; }

        public string ObjectJson { get; set; }
    }
}