﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WMS.Api.DAL.Models.ApiAuthModels;

namespace WMS.Api.DAL.Models.TimeLogModels
{
    public class TimeLogEntry: Entity
    {
        [Required]
        public Guid UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        
        public Guid? UpdateUserId { get; set; }

        [ForeignKey("UpdateUserId")]
        public virtual ApplicationUser UpdateUser { get; set; }

        [Required]
        public DateTime EntryTime { get; set; }

        public DateTime? LeaveTime { get; set; }

        [Required]
        public bool IsApproved { get; set; }

        [Required]
        public Guid TimeLogEntryTypeId { get; set; }

        [ForeignKey("TimeLogEntryTypeId")]
        public virtual TimeLogEntryType TimeLogEntryType { get; set; }

        [Required]
        public Guid AppLogId { get; set; }

        [ForeignKey("AppLogId")]
        public virtual AppLog AppLog { get; set; }
    }
}