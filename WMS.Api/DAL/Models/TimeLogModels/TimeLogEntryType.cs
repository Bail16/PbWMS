﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WMS.Api.DAL.Models.TimeLogModels
{
    public class TimeLogEntryType: Entity
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public Int16 ReduceTime { get; set; }

        [Required]
        public bool NeedsConfirmation { get; set; }

        [Required]
        public bool IsSelectable { get; set; }

        [Required]
        public string CamelCaseName { get; set; }
    }
}