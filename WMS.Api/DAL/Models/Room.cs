﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WMS.Api.DAL.Models
{
    public class Room : Entity
    {
        [Required]
        public string RoomName { get; set; }

        public virtual ICollection<RoomReservation> Reservations { get; set; }
    }
}