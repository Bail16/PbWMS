﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace WMS.Api.DAL.Models
{
    public class Entity
    {
        public Entity()
        {
            Id = Guid.NewGuid();
            IsDeleted = false;
            Timestamp = DateTime.Now;
        }
        public Guid Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? Timestamp { get; set; }
    }
}